#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QPushButton>
#include <QGraphicsView>
#include <QGraphicsItemGroup>
#include <QLabel>
#include <QMessageBox>
#include <QDebug>
#include <QSignalMapper>
#include <QGridLayout>
#include <QShortcut>
#include "player.h"
#include "bank.h"
#include "tiles.h"
#include "time.h"
#define numOfPlayers 3
#define userLocation 0
#define ai1Location  1
#define ai2Location  2

namespace Ui {
class MainWindow;
}
class MainWindow : public QMainWindow
{
    Q_OBJECT

    friend class states;
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    //connections
    void connections(int);
    void signalMapping(QSignalMapper*,QPushButton*);
    void signalMapping(QSignalMapper *mapped,QShortcut *sc);
    void setupShortcuts();
    //deallocation
    void deallocation(QPushButton*);
    //set up buttons
    void setPushButton(QPushButton*,QString,int,int,int,int,bool);
    void modifyPushButton(QPushButton* ,int,int,int,int);
    //setup images
    void setImage(QString,int,int);
    void resizeImage(QPixmap,int,int);
    void setUpScene();
    void displayCard(QString);

    //setup tiles
     void setProductionTileHolder();
    //Message boxes
    void messageBox(QString);
    bool QuestionMessageBox(QString);
    void setBox(QString);
    //setters
    void setResourceWidget(player*);
    void resourcesSetting();
    void initalStart();
    void initStartPlayer();
    void setResourcesAndBoard();
    void setupProductionTiles();
    QString setTaskInfo();
    //grid windows
    void gridWindow(QVector<QString>,int);
    void gridWindowWithInfo(QVector<QString>,int);
    void gridWindowDeckType();
    void displayPermDeck();
    //modifications
    void modifyLabel(QLabel*,bool,int,int,int,int);
    void labelSize(QLabel*,int,int,int,int);
    void addText2Label(QLabel*,QString,QString);
    void addImage2Label(QLabel*,QPixmap,int,int);
    void updateResourceWidget(player*);
    void removeResourceWidget();
    void deleteResourceObj();
    void removeTileBoard();
    void showTileBoard();
    void setupResourceInfo(QGraphicsRectItem*);
    //getters
    QString getCard();
    QString getCardInfo(QString,int);
    void getCulture();
    void acquireCurrentHand();
    void acquirePermCard(QPushButton*,int);
    void acquireCurrentCard(QPushButton*,int);
    QString getActionCardImage(QString,QString);
    //sorting
    void sortProductionTiles();
    int randAiTile();
    int randAiTile20();
    void randAi1Turn();
    void randAi2Turn();
    //displays
    void showBoards(player*,QString);
    void displayVictoryCards();
    void title();
    void dispCard(int);
    void dispCard(QString,QString,QString);
    void dispCard(QString,QString,QString,QString);
    void displayResourcesCost(player*);
    void displayResourceTrade(player*);
    void displayResourceUpdated(player*);
    //various functions
    int cardEqualTerrain(int,QString);
    void cardEqualTerrain(int,player*);
    void whoIsPlaying();
    void currentTurn();
    void deletedisp();
    void addProductionTiles();
    void spoilage();
 //functions that are called when signals are emitted based
// on their connect
public slots:
    void stateMachine();
    void pickResourceProductions();
    void afterTitle();
    void cultureChosen();
    void switchBoards(int);
    void selectProductionTile(int);
    void skipPlayer();
    void placeTile();
    void updateGridInfo(int);
    void nextTurnForVictoryCards();
    void deckSelected(int);
    void placeIntoCurrentHand(int);
    void pickRandomCards();
    void setUpNextState();
    void showCurrentHand();
    void infoAboutCard(int);
    void closeCurrentHand();
    void doCardAction();
    void incPlayer();
    void readStatus();
    void takeResource(int id);
    void ResourceTrade(int);
    void updateWantedResources();
    void add2Resources(int);
    void finshedAction();
//various signals that are are not
//from Qt
signals:
    void nextState();
    void nextPlayer();
    void startTurn();
    void done();
private:
    //Main window data
    Ui::MainWindow *ui;
    QSignalMapper *map;
    QShortcut *a1, *a2,*u,*uBoard,*ai1Board,*ai2Board;
    //bank object
    bank *gameBank;
    //tile object
    tiles *tilesClass;
    int stateFlag=0;
    //image scenes
    QGraphicsView *view;
    QGraphicsScene *scene;
    //In order: Production tile,victory cards
    QWidget *displayWindow,*displayCards,*deckTypeDisplay,*displayPerm,*displayCurrent;
    QGridLayout *grid;
    //Buttons
    QPushButton *start,*cont,*ai1,*ai2,*userButton,*pass,*tile2,*currentHand,*ok;
    //labels
    QLabel *popup,*popup2,*warningBox,*wonder,*winning,*army,*building,*cardInfo;
    //messageboxes
     QMessageBox *message;
     //cultures
     int cultures[3];
    //directory
     //QString const dir = "C:/Users/sir_pirate/Desktop/Class/343/Project/soureCodes/AgeOfMeth/pictures";
     //QString const dir = "E:/School/Spring_2015/CECS_343/Git/Git2/cec343_finalproject/soureCodes/AgeOfMeth/pictures";
     QString const dir = ":/pictures";
     QString const Egyptcultcard = dir+"/egyp/CULTURE_E.jpg";
     QString const GreekCultCard  = dir+"/Greek/CULTURE_G.jpg";
     QString const NorseCultCard = dir+"/norse/Norse_Culture.jpg";
     QString const randmHeavy = dir+"mikey.jpg";
     //players
     player *user,*ai_1,*ai_2;
     QVector<player*> initialOrderOfPlayers, startingOrderOfPlayers,last;
     //resources
      QVector<int> resources;
     QGraphicsRectItem *boarderBox,*food,*victory,*favor,*gold,*wood;
     QGraphicsSimpleTextItem *foodText,*victoryText,*favorText;
     QGraphicsSimpleTextItem *goldText,*woodText,*titleText;
     //Production tiles
      int flip=0,numLoops=0,chosenTileCounter=0,numClicks=0,TileIdtoLocation,tempID,startingPlayerIndex=0,tempAIClicks=0;
      bool isDone;
      QString CardHolder;
      QVector<QPushButton*> tile,tilePlaceHolder,victoryCardHolders;
      QVector<QString> ProdTiles,tempHolderForProductionTiles, victoryCards;
      QVector<QString> directoryList,terrainType,resourceProduced,numResourcesProduced,tempVect;
        //Victory Cards
      int buildingVictoryCubes=0,armyVictoryCubes=0,winningVictoryCubes=0;
      int wonderVictoryCubes=0;
      //QString const victoryDir = "C:/Users/sir_pirate/Desktop/Class/343/Project/soureCodes/AgeOfMeth/pictures/VictoryCards";
      //QString const victoryDir = "E:/School/Spring_2015/CECS_343/Git/Git2/cec343_finalproject/soureCodes/AgeOfMeth/pictures/VictoryCards";
      QString const victoryDir = ":/pictures/VictoryCards";
      //Chosing Cards
      int deckId;
      QVector<QPushButton*> permHolder,deckTypeHolder;
      //action card draw
      QPushButton *permDone;
      QVector<QPushButton*> currentHolder;
      int currentPlayingActionCard,randomExplore,cardRank,ExploreCount=0;
      //display Resources
      QWidget *dispResource,*dispResource1,*dispResource2;
      QVector<QPushButton*> resourceHolder;
      QVector<int> pickedResources;
      QLabel *foodT,*woodT,*goldT,*favorT,*victT;
      int cost,acknowledgeCost,totalAmt;
      QButtonGroup *groupCost,*groupTrade,*groupUpdated;
      QPushButton *doneTrade,*doneUpdated;
      //spoilage
      QVector<Resource> listValues;

};



#endif // MAINWINDOW_H
