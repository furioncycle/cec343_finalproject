#ifndef	HOLDINGAREA_H
#define HOLDINGAREA_H

#include <list>
#include "bank.h"
using namespace std;

class holdingArea{

public:
    holdingArea(): userGold(0), userWood(0), userFood(0), userFavor(0)
                    , userVillagers(0), userVictoryPoints(0) {}
    ~holdingArea(){}

    int getGold(int g);
    int getFavor(int f);
    int getFood(int fd);
    int getWood(int w);
    int getVillagers(int v);
    int getPointCubes(int vp);
   // void setGold();
   // void setFavor();
   // void setFood();
   // void setWood();
   // void setVillagers();
   // void setPointCubes();
   // void setResources();

    int CheckResc(Resource rescType);
    void AddResc(Resource rescType, int amount);
    int RemoveResc(Resource rescType, int amount);
private:
    int userGold,userFood,userWood,userFavor;
    int userVillagers,userVictoryPoints,temp;

}; //end holdingArea class

/*class victoryCard : public holdingArea{


    string name;

public:
    victoryCard();
    ~victoryCard();

    void getName(string n);

}; //end victoryCard class*/

#endif
