/*
    This file contains the definitions for the member functions of
    the system class.
*/
#include <iostream>
#include "System.h"
#include "player.h"

/*
    Default constructor for the System class. Initializes the game with all the
    necessary information to start an AOM game.
*/
System::System() {
    // Load Players (User, AI_1, AI_2)
    player* user = new player("User", NORSE);
    player* ai_1 = new player("AI_1", GREEK);
    player* ai_2 = new player("AI_2", EGYPTIAN);

    // Add the players to the deque in the determined order
    // Currently not randomized
    players.push_back(user);
    players.push_back(ai_1);
    players.push_back(ai_2);

    // Load Bank
    systemBank = new bank();

    // Distribute initial resources
    for(int index = 0; index < players.size(); ++index) {
        WithdrawFromBank(index, GOLD, 4);
        WithdrawFromBank(index, FAVOR, 4);
        WithdrawFromBank(index, WOOD, 4);
        WithdrawFromBank(index, FOOD, 4);
    }

    // Load resource pool

    // Wait for initial resource tile selection

}


/*
    Destructor for the System class. Deallocates memory for each player and the bank.
*/
System::~System() {
    // deallocate players
    while(!players.empty()) {
        if(players.back() != nullptr) {
            delete players.back();
        }
        players.pop_back();
    }

    // deallocate bank
    if(systemBank != nullptr) {
        delete systemBank;
    }
}

/*
    Move the indicated amount of resources from the bank to the player at the
    indicated index in the player deque.

    @param playerIndex The index of the player that will recieve the resources.
    @param rescType The type of resource to be withdrawn.
    @param rescAmt The amount of resource to be withdrawn.

    @return The amount of resources actually transferred if bank was depleted.
*/
int System::WithdrawFromBank(int playerIndex, Resource rescType, int rescAmt) {
    int remainingResc = 0;

    // Remove resource from bank
    remainingResc = systemBank->RemoveResc(rescType, rescAmt);

    // Add resources from bank to the indicated player
    this->players[playerIndex]->AddResc(rescType, rescAmt);

    // Return the amount of resources transferred
    return remainingResc;
}


/*
    Returns the player's current amount of the indicated resource.

    @param playerIndex The index of the player in the System's deque.
    @param rescType The type of resource to check.

    @return The amount of the indicated resource in the player's holding area.
*/
int System::CheckPlayerResc(int playerIndex, Resource rescType) {
    return players[playerIndex]->CheckResc(rescType);
}


/*
    Performs the action of the card at the indicated index in the player's hand.

    @param playerIndex The index of the player in the System's deque.
    @param cardIndex The index of the card in the player's hand.
*/
void System::PerformPlayerAction(int playerIndex, int cardIndex) {
    this->players[playerIndex]->PerformSelectedAction(cardIndex);
}

void System::PlayerDrawPermanent(int playerIndex, int cardIndex) {
    this->players[playerIndex]->DrawPermanent(cardIndex);
}
