#include "mainwindow.h"
#include <QApplication>
#include <QStyleFactory>
#include <iostream>
#include "System.h"
#include "player.h"

int main(int argc, char *argv[])
{

    QApplication a(argc, argv);
    QApplication::setStyle(QStyleFactory::create("Fusion"));
    //set up information on program and users
    QApplication::setOrganizationDomain("com.CSULB.CECS343.Group7");
    QApplication::setOrganizationName("Group 7");
    QApplication::setApplicationName("Age of METH");
    QApplication::setApplicationVersion("1.0.0");
    MainWindow w;
    w.showMaximized();
    return a.exec();

    // Josh test code

    // Initialize System
  //  System* gameSystem = new System();

    // Test that players recieved resources
  //  std::cout << gameSystem->CheckPlayerResc(0, GOLD) << std::endl; // User Gold

//    gameSystem->PlayerDrawPermanent(0, 1);
//    gameSystem->PerformPlayerAction(0, 0);


  //  delete gameSystem;

}
