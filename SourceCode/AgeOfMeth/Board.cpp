#include "Board.h"
#include <QDebug>
#include <iostream>

Board::Board(){
    PlayerHoldingArea = new holdingArea();
}

Board::~Board(){
    // deallocate holding area
    if(PlayerHoldingArea != nullptr) {
        delete PlayerHoldingArea;
    }
}

QString Board::getCultureBoard(){
        return fullBoard;
}

QString Board::getCityArea(){
    qDebug()<<cityArea;
    return cityArea;
}
QString Board::getProductionArea(){
    return ProductionArea;
}

QString Board::getHoldingArea(){
    return hold;
}

void Board::setCultureBoard(QString culture){
    if( culture == "Egyptian"){
        fullBoard = EgyptBoardGame;
        hold = EqyptHoldingArea;
        cityArea = EqyptCityArea;
        ProductionArea = EqyptProductionArea;

    }
    else if(culture == "Greek"){
        fullBoard = GreekBoardGame;
        hold = GreekHoldingArea;
        cityArea = GreekCityArea;
        ProductionArea = GreekProductionArea;
    }
    else if(culture == "Norse"){
        fullBoard = NorseBoardGame;
        hold = NorseHoldingArea;
        cityArea = NorseCityArea;
        ProductionArea = NorseProductionArea;
     }
    else
        qDebug()<<"Error";
}

void Board::setProductionArea(QString culture){
    if(culture == "Egyptian"){
        ProductionAreaTerrain << "DESERT" << "DESERT" << "SWAMP" << "SWAMP"
                              << "FOREST" << "DESERT" << "FERTILE" << "FERTILE"
                              << "DESERT" << "DESERT" << "FERTILE" << "FERTILE"
                              << "DESERT" << "HILL" << "FERTILE" << "HILL";
     }
    else if(culture == "Greek"){
        ProductionAreaTerrain << "FERTILE" << "FERTILE" << "FOREST" << "SWAMP"
                              << "HILL" << "MOUNTAIN" << "FERTILE" << "FOREST"
                              << "HILL" << "HILL" << "HILL" << "HILL"
                              << "DESERT" << "HILL" << "HILL" << "HILL";
    }
    else if(culture == "Norse"){
        ProductionAreaTerrain << "FERTILE" << "MOUNTAIN" << "MOUNTAIN" << "MOUNTAIN"
                              << "FERTILE" << "FOREST" << "HILL" << "MOUNTAIN   "
                              << "HILL" << "SWAMP" << "FOREST" << "HILL"
                              << "DESERT" << "FOREST" << "FOREST" << "HILL";
    }
}

QVector<QString> Board::getProductionAreaTerrain(){
        return ProductionAreaTerrain;
}


/*
     Returns the resource amount that the player currently has for the indicated resource.
     If anything goes wrong, return -1.

     @param rescType The type of resource to be returned.
     @return The amount of resource that the player currently has.
*/
int Board::CheckResc(Resource rescType) {
    return this->PlayerHoldingArea->CheckResc(rescType);
}


/*
    Adds the indicated amount of resource to the player's holding area.

    @param rescType The type of resource to be added.
    @param amount The amount of resource to be added.
*/
void Board::AddResc(Resource rescType, int amount) {
    this->PlayerHoldingArea->AddResc(rescType, amount);
}


/*
    Removes the indicated amount of resource from the player's holding area. If there is not enough
    resources available, then the remaining is depeted and the actual amount returned is removed.

    @param rescType The type of resource to be removed.
    @param amount The amount of resource to be removed.
    @return The actual amount removed from the holding area.
*/
int Board::RemoveResc(Resource rescType, int amount) {
    return this->PlayerHoldingArea->RemoveResc(rescType, amount);
}

