/*
	This file contains the declarations of the member functions for the ActionCard classes.
*/

#include <iostream>
#include "player.h"
#include "ActionCard.h"
using namespace std;


//-----------------------------------------------------------------------------
// NEXT AGE CARD
//-----------------------------------------------------------------------------

/*
	Performs the indicated action based on the card's rank
*/
int NextAgeCard::PerformCardAction() {
    int finishedType;
	switch (this->GetRank()) {
	case GOD:
     finishedType=   this->PerformGodAction();
        break;
	case RANDOM:
      finishedType=  this->PerformRandomAction();
        break;
	default: // default to the permanent action card
      finishedType=  this->PerformPermanentAction();
        break;
	}
    return finishedType;
}

/*
	Performs the action for a Next Age Permanent Action Card.
*/
int NextAgeCard::PerformPermanentAction() {
    qDebug()<<"Performing next age card";
    // Check that the player has the appropriate resources based on their current age
    int rescReq = 0;

    switch(owner->ReturnAge()) {
    case ARCHAIC:
        rescReq = 4;
        break;
    case CLASSICAL:
        rescReq = 5;
        break;
    case HEROIC:
        rescReq = 6;
        break;
    default:
        // already at max age, so do nothing
        return -1;
    }

    if( owner->CheckResc(GOLD) >= rescReq
     && owner->CheckResc(FAVOR) >= rescReq
     && owner->CheckResc(FOOD) >= rescReq
     && owner->CheckResc(WOOD) >= rescReq
    ) {
        // deduct the resources from the player
        owner->RemoveResc(GOLD, rescReq);
        owner->RemoveResc(FOOD, rescReq);
        owner->RemoveResc(FAVOR, rescReq);
        owner->RemoveResc(WOOD, rescReq);
    }
    else {
        // do nothing
        return -1;
    }

    // advance the player to the next age
    switch(owner->ReturnAge()) {
    case ARCHAIC:
        owner->setCurrentAge(CLASSICAL);
        break;
    case CLASSICAL:
        owner->setCurrentAge(HEROIC);
        break;
    case HEROIC:
        owner->setCurrentAge(MYTHIC);
        break;
    default:
        // already at max age, so do nothing
        return -1;
    }

    // display the player's new age
    std::cout << owner->ReturnAge() << std::endl;
    //they were able to increment to next age
    return 1;
}

/*
	Performs the action for a Next Age Random Action Card.
*/
int NextAgeCard::PerformRandomAction() {
    qDebug()<<"Performing next age  randcard";
    this->PerformPermanentAction();
    return 1;
}

/*
    Performs the action for a Next Age God Action Card.
*/
int NextAgeCard::PerformGodAction() {
    qDebug()<<"Performing next age rand god card";
    this->PerformPermanentAction();
    return 1;
}



//-----------------------------------------------------------------------------
// GATHER CARD
//-----------------------------------------------------------------------------

/*
    Performs the indicated action based on the card's rank
*/
int GatherCard::PerformCardAction() {
    qDebug()<<"Performing gather card";
    int finishedType;
	switch (this->GetRank()) {
	case GOD:
      finishedType=  this->PerformGodAction();
        break;
	case RANDOM:
      finishedType=  this->PerformRandomAction();
        break;
	default: // default to the permanent action card
       finishedType= this->PerformPermanentAction();
        break;
	}
    return finishedType;
}

/*
    Performs the action for a Gather Permanent Action Card.
*/
int GatherCard::PerformPermanentAction() {
    qDebug()<<"Performing gather perm card";
	cout << "Gather Permanent Action Card" << endl;
    return 1;
}

/*
    Performs the action for a Gather Random Action Card.
*/
int GatherCard::PerformRandomAction() {
    qDebug()<<"Performing gather rand  card";
    cout << "Gather Random Action Card" << endl;
    return this->PerformPermanentAction();
}

/*
    Performs the action for a Gather God Action Card.
*/
int GatherCard::PerformGodAction() {
    cout << "Gather God Card: " << this->GetName() << endl;
    // Not implemented, perform permanent action
      return this->PerformPermanentAction();
}



//-----------------------------------------------------------------------------
// BUILD CARD
//-----------------------------------------------------------------------------

/*
    Performs the indicated action based on the card's rank
*/
int BuildCard::PerformCardAction() {
    qDebug()<<"Performing build card";
    int finishedType;
	switch (this->GetRank()) {
	case GOD:
      finishedType=  this->PerformGodAction();
        break;
	case RANDOM:
     finishedType=   this->PerformRandomAction();
        break;
	default: // default to the permanent action card
      finishedType=  this->PerformPermanentAction();
        break;
    }
    return finishedType;
}

/*
    Performs the action for a Build Permanent Action Card.
*/
int BuildCard::PerformPermanentAction() {
	cout << "Build Permanent Action Card" << endl;
    return 1;
}

/*
    Performs the action for a Build Random Action Card.
*/
int BuildCard::PerformRandomAction() {
    cout << "Build Random Action Card" << endl;
    // Not implemented
       return this->PerformPermanentAction();
}

/*
    Performs the action for a Build God Action Card.
*/
int BuildCard::PerformGodAction() {
    cout << "Build God Card: " << this->GetName() << endl;
    // Not implemented
       return this->PerformPermanentAction();
}



//-----------------------------------------------------------------------------
// TRADE CARD
//-----------------------------------------------------------------------------

/*
    Performs the indicated action based on the card's rank
*/
int TradeCard::PerformCardAction() {
    qDebug()<<"Performing trade card";
    int finishedType;
	switch (this->GetRank()) {
	case GOD:
      finishedType=  this->PerformGodAction();
        break;
	case RANDOM:
      finishedType=  this->PerformRandomAction();
        break;
	default: // default to the permanent action card
      finishedType=  this->PerformPermanentAction();
        break;
	}
    return finishedType;
}

/*
    Performs the action for a Trade Permanent Action Card.
*/
int TradeCard::PerformPermanentAction() {

    // Deduct the amount of resources necessary to play the trade card
    // Cost is any two resources
    // Player selects which resources to deduct

    // Need user to select which resources to spend for cost
   Resource rescType1 = WOOD; // hard coded for now
    Resource rescType2 = FOOD; // hard coded for now

    if(rescType1 == rescType2) {
        if(owner->CheckResc(rescType1) >= 2) {
            // deduct resource
            owner->RemoveResc(rescType1, 2);
        }
        else {
            // return with error
            return -1;
        }
    }
    else {
        if(owner->CheckResc(rescType1) > 0 && owner->CheckResc(rescType2) > 0) {
            // deduct resources
            owner->RemoveResc(rescType1, 1);
            owner->RemoveResc(rescType2, 1);
        }
        else {
            // return with error
            return -1;
        }
    }

    // Player selects how many of each type of cube they would like to return to the bank
    int woodAmt = 0;
    int foodAmt = 0;
    int favorAmt = 0;
    int goldAmt = 0;

    // user input needed, hard coded for now
    woodAmt = 1;
    foodAmt = 1;
    favorAmt = 1;
    goldAmt = 0;

    // check if resources are available
    if(owner->CheckResc(WOOD) < woodAmt) {
        return -1;
    }
    else if(owner->CheckResc(FOOD) < foodAmt) {
        return -1;
    }
    else if(owner->CheckResc(GOLD) < goldAmt) {
        return -1;
    }
    else if(owner->CheckResc(FAVOR) < favorAmt) {
        return -1;
    }
    else {
        // deduct resources and add to bank
        owner->RemoveResc(WOOD, woodAmt);
        owner->RemoveResc(FAVOR, favorAmt);
        owner->RemoveResc(GOLD, goldAmt);
        owner->RemoveResc(FOOD, foodAmt);

        // need to add to bank
    }

    int totalResc = woodAmt + foodAmt + favorAmt + goldAmt;

    // Player selects the amount of each type of resource that they would like
    int playerWood = 0;
    int playerFood = 0;
    int playerGold = 0;
    int playerFavor = 0;

    // hard code the user's desired amounts for now
    playerWood = 0;
    playerFood = 0;
    playerGold = 3;
    playerFavor = 0;

    // The total selected cannot exceed the amount returned to the bank
    if(playerWood + playerFood + playerGold + playerFavor > totalResc) {
        return -1;
    }

    // need to check to see if the amounts are available in the bank
    // deduct from bank

    // Add the resources to the player's holding area
    owner->AddResc(FOOD, playerFood);
    owner->AddResc(WOOD, playerWood);
    owner->AddResc(FAVOR, playerFavor);
    owner->AddResc(GOLD, playerGold);

    return 1; // successful
}

/*
    Performs the action for a Trade Random Action Card.
*/
int TradeCard::PerformRandomAction() {
    cout << "Trade Random Action Card" << endl;
    // Not implemented
    return this->PerformPermanentAction();
}

/*
    Performs the action for a Trade God Action Card.
*/
int TradeCard::PerformGodAction() {
    cout << "Trade God Card: " << this->GetName() << endl;
    // Not implemented
    return this->PerformPermanentAction();
}



//-----------------------------------------------------------------------------
// EXPLORE CARD
//-----------------------------------------------------------------------------

/*
    Performs the indicated action based on the card's rank
*/
int ExploreCard::PerformCardAction() {
    qDebug()<<"Performing explore card";
    int finishedType;
	switch (this->GetRank()) {
	case GOD:
       finishedType= this->PerformGodAction();
        break;
	case RANDOM:
       finishedType= this->PerformRandomAction();
        break;
	default: // default to the permanent action card
     finishedType=   this->PerformPermanentAction();
        break;
	}
    return finishedType;
}

/*
    Performs the action for a Explore Permanent Action Card.
*/
int ExploreCard::PerformPermanentAction() {
    cout << "Explore Permanent Action Card" << endl;
    return 1;
}

/*
    Performs the action for a Explore Random Action Card.
*/
int ExploreCard::PerformRandomAction() {
    cout << "Explore Random Action Card" << endl;
    return 1;
}

/*
    Performs the action for a Explore God Action Card.
*/
int ExploreCard::PerformGodAction() {
    cout << "Explore God Card: " << this->GetName() << endl;
    return 1;
}
