#include <iostream>
#include <list>
#include <string>
#include "holdingarea.h"

using namespace std;


/*
    Returns the amount of the indicated resource. Returns -1 if anything goes wrong.

    @param rescType The type of resource to be returned.
    @return The amount of the indicated resource in the holding area.
*/
int holdingArea::CheckResc(Resource rescType) {
    // Return the indicated resource amount, if anything goes wrong return -1
    switch(rescType) {
    case WOOD:
        return userWood;
        break;
    case FOOD:
        return userFood;
        break;
    case FAVOR:
        return userFavor;
        break;
    case GOLD:
        return userGold;
        break;
    case VICTORY:
        return userVictoryPoints;
        break;
    default:
        return -1;
    }

    return -1;
}


/*
    Adds the indicated amount to the specified resource.

    @param rescType The type of resource to be added.
    @param amount The amount of resource to be added.
*/
void holdingArea::AddResc(Resource rescType, int amount) {
    // Add the indicated amount of resource
    switch(rescType) {
    case WOOD:
        userWood += amount;
        break;
    case FOOD:
        userFood += amount;
        break;
    case FAVOR:
        userFavor += amount;
        break;
    case GOLD:
        userGold += amount;
        break;
    case VICTORY:
        userVictoryPoints += amount;
        break;
    default:
        return;
    }
}


/*
    Removes the indicated amount of resource from the holding area. If the specified
    amount is not available, then the remaining amount will be depleted. The actual amount
    removed will be the return value.

    @param rescType The type of resource to be removed.
    @param amount The amount of resource to be removed.
    @return The actual amount of resource removed from the holding area.
*/
int holdingArea::RemoveResc(Resource rescType, int amount) {
    int depletedAmount = 0;

    // if there is not enough available, then return the amount in the holding area
    switch(rescType) {
    case WOOD:
        if(userWood < amount) {
            depletedAmount = userWood;
            userWood = 0; // deplete bank
            return depletedAmount; // return amount
        }
        else {
            userWood -= amount; // enough in bank
        }
        break;
    case FOOD:
        if(userFood < amount) {
            depletedAmount = userFood;
            userFood = 0;
            return depletedAmount;
        }
        else {
            userFood -= amount;
        }
        break;
    case FAVOR:
        if(userFavor < amount) {
            depletedAmount = userFavor;
            userFavor = 0;
            return depletedAmount;
        }
        else {
            userFavor -= amount;
        }
        break;
    case GOLD:
        if(userGold < amount) {
            depletedAmount = userGold;
            userGold = 0;
            return depletedAmount;
        }
        else {
            userGold -= amount;
        }
        break;
    case VICTORY:
        if(userVictoryPoints < amount) {
            depletedAmount = userVictoryPoints;
            userVictoryPoints = 0;
            return depletedAmount;
        }
        else {
            userVictoryPoints -= amount;
        }
        break;
    default:
        return -1; // something went wrong
    }

    return amount; // all requested resources removed
}


int holdingArea::getGold(int g) {
    temp = userGold;
    if((temp-g)< 0)
        return -1;
    else
        return userGold - g;
}

int holdingArea::getFavor(int f){
    temp = userFavor;
    if((temp-f)< 0)
        return -1;
    else
        return userFavor - f;
}

int holdingArea::getFood(int fd){
    temp = userFood;
    if((temp-fd)< 0)
        return -1;
    else
        return userFood - fd;
}

int holdingArea::getWood(int w){
    temp = userWood;
    if((temp-w)< 0)
        return -1;
    else
        return userWood - w;
}

int holdingArea::getVillagers(int v){
    temp = userVillagers;
    if((temp-v)< 0)
        return -1;
    else
        return userVillagers - v;
}

int holdingArea::getPointCubes(int vp){
    temp = userVictoryPoints;
    if((temp-vp)< 0)
        return -1;
    else
        return userVictoryPoints - vp;
}
/*
void holdingArea::setGold(){
    userGold = b.golds;
}

void holdingArea::setFavor(){
    userFavor = favors ;
}

void holdingArea::setFood() {
   userFood =  b.foods;
}

void holdingArea::setWood() {
    userWood = b.woods;
}

void holdingArea::setVillagers() {
    userVillagers =0;
}

void holdingArea::setPointCubes() {
    userVictoryPoints = b.victoryPointCubes;
}

void holdingArea::setResources(){
    setFavor(); setFood(); setGold();
    setPointCubes(); setVillagers();
    setWood();
}

int victoryCard::getName(string n) {
    name = n;
    cout << "Name is " << n << endl;
}*/



