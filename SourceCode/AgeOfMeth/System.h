/*
    This file contains the class declaration for the system class.
*/

#ifndef SYSTEM_H
#define SYSTEM_H

#include <deque>
#include <string>
#include "bank.h"

// Forward Declarations
class player;

/*
    The system class controls all the logic to run the AOM game.
*/
class System {
public:
    // Constructor
    System();

    // Destructor
    ~System();

    int WithdrawFromBank(int playerIndex, Resource rescType, int rescAmt);
    int CheckPlayerResc(int playerIndex, Resource rescType);
    void PerformPlayerAction(int playerIndex, int cardIndex);
    void PlayerDrawPermanent(int playerIndex, int cardIndex);

private:
    std::deque<player*> players; // deque of players in the game
    bank* systemBank;
};

#endif // SYSTEM
