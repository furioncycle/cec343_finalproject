#include <iostream>
#include <fstream>
#include <algorithm>
#include <ctime>
#include <cstdlib>
#include <vector>
#include <QString>
#include <QFile>
#include <QList>
#include <QByteArray>
#include "player.h"
#include "ActionCard.h"
using namespace std;

/*
    Constructor that initializes a player using the indicated culture. Assigns a player the
    appropriate action card decks.
*/
player::player(QString playerName, culture playerCulture) {
    this->playerName = playerName;
    this->Page = ARCHAIC;
    this->currentAge = "Archaic";
    this->Pculture = playerCulture;
    this->gameboard = new Board();

    // Create a deck of the 5 permanent action cards (excluding battle)
    //permanentDeck.push_back(new BuildCard(this));
    permanentDeck.push_back(new TradeCard(this));
    permanentDeck.push_back(new NextAgeCard(this));
    permanentDeck.push_back(new ExploreCard(this));
    permanentDeck.push_back(new NextAgeCard(this)); // Place holder for GATHER
    permanentDeck.push_back(new ExploreCard(this)); // Place holder for BUILD
    //permanentDeck.push_back(new GatherCard(this));

    // Read the appropriate text file and create the necessary random action cards
    //ifstream actionCardFile;

    std::string cardName = "";
    QFile actionCardFile;

    int cardRank = 0;
    int buildNum = 0;

    switch(playerCulture) {
    case NORSE:
        //actionCardFile.open("E:/School/Spring_2015/CECS_343/AOM_Qt/Initialize/RandomActionCards_Norse.txt");
        //actionCardFile.open(dir::mainDir + "/Initialize/RandomActionCards_Norse.txt");
        actionCardFile.setFileName(":/Initialize/RandomActionCards_Norse.txt");
        break;
    case GREEK:
        //actionCardFile.open("E:/School/Spring_2015/CECS_343/AOM_Qt/Initialize/RandomActionCards_Greek.txt");
        //actionCardFile.open(dir::mainDir + "/Initialize/RandomActionCards_Greek.txt");
        actionCardFile.setFileName(":/Initialize/RandomActionCards_Greek.txt");
        break;
    case EGYPTIAN:
        //actionCardFile.open("E:/School/Spring_2015/CECS_343/AOM_Qt/Initialize/RandomActionCards_Egyptian.txt");
        //actionCardFile.open(dir::mainDir + "/Initialize/RandomActionCards_Egyptian.txt");
        actionCardFile.setFileName(":/Initialize/RandomActionCards_Egyptian.txt");
        break;
    default:
        // throw an error???
        break;
    }

    // if no file was opened, then display error message
    if(!actionCardFile.open(QIODevice::ReadOnly)) {
        std::cout << "The Random Action Card input file was not found." << std::endl;
    }

    QList<QByteArray> lineDetails;

    // loop until the file is empty
    //while(actionCardFile >> cardName >> cardRank >> buildNum) {
    while(!actionCardFile.atEnd()) {
        lineDetails = actionCardFile.readLine().split(' ');

        cardName = lineDetails[0].toStdString();
        cardRank = lineDetails[1].toInt();
        buildNum = lineDetails[2].toInt();

        // add a card for each line in the input file
        if(cardName == "TRADE" ||
            cardName == "HERMES" || // Greek
            cardName == "FORSETI" || cardName == "LOKI") { // Norse
            randomDeck.push_back(new TradeCard(cardName, (CardRank)cardRank, this));
        }
        else if(cardName == "GATHER" ||
            cardName == "POSEIDON" || cardName == "HADES" || cardName == "DIONYSUS" || // Greek
            cardName == "RA" || // Egyptian
            cardName == "THOR" || cardName == "SKADI" || cardName == "FREYJA") { // Norse
            //randomDeck.push_back(new GatherCard(cardName, (CardRank)cardRank, this));
        }
        else if(cardName == "NEXT_AGE" ||
            cardName == "ZEUS" || cardName == "HEPHAESTOS" || // Greek
            cardName == "SET" || cardName == "HATHOR" || // Egyptian
            cardName == "ODIN") { // Norse
            randomDeck.push_back(new NextAgeCard(cardName, (CardRank)cardRank, this));
        }
        else if(cardName == "EXPLORE" ||
            cardName == "ARTEMIS" || //Greek
            cardName == "PTAH" || // Egyptian
            cardName == "BALDR") { // Norse
            randomDeck.push_back(new ExploreCard(cardName, (CardRank)cardRank, this));
        }
        else if(cardName == "BUILD" ||
            cardName == "HERA" || // Greek
            cardName == "NEPHTHYS" || cardName == "HORUS" || // Egyptian
            cardName == "NJORD") { // Norse
            //randomDeck.push_back(new BuildCard(cardName, (CardRank)cardRank, buildNum, this));
        }
        else {
            // Card not recognized, display error
            std::cout << cardName << " not recognized." << std::endl;
        }
    } // end while

    // Close the file
    actionCardFile.close();

    // Shuffle the random action card deck
    this->Shuffle();

}

/*
    Destructor for the player class. Deallocates memory for any cards in the player's deck.
*/
player::~player(){
    // Deallocate permanent action card deck
    while(!permanentDeck.empty()) {
        if(permanentDeck.back() != nullptr) {
            delete permanentDeck.back();
        }
        permanentDeck.pop_back();
    }

    // Deallocate random action card deck
    while(!randomDeck.empty()) {
        if(randomDeck.back() != nullptr) {
            delete randomDeck.back();
        }
        randomDeck.pop_back();
    }

    // deallocate memory for the player's board
    if(gameboard != nullptr) {
        delete gameboard;
    }
}


/*
    Initializes the player's action card decks based on culture.
*/
void player::InitializeCards() {
    // get the player's culture
    culture playerCulture = this->Pculture;

    // Create a deck of the 5 permanent action cards (excluding battle)
    //permanentDeck.push_back(new BuildCard(this));
    permanentDeck.push_back(new TradeCard(this));
    permanentDeck.push_back(new NextAgeCard(this));
    permanentDeck.push_back(new ExploreCard(this));
    permanentDeck.push_back(new NextAgeCard(this)); // Place holder for GATHER
    permanentDeck.push_back(new ExploreCard(this)); // Place holder for BUILD
    //permanentDeck.push_back(new GatherCard(this));

    // Read the appropriate text file and create the necessary random action cards
    //ifstream actionCardFile;

    std::string cardName = "";
    QFile actionCardFile;

    int cardRank = 0;
    int buildNum = 0;

    switch(playerCulture) {
    case NORSE:
        //actionCardFile.open("E:/School/Spring_2015/CECS_343/AOM_Qt/Initialize/RandomActionCards_Norse.txt");
        //actionCardFile.open(dir::mainDir + "/Initialize/RandomActionCards_Norse.txt");
        actionCardFile.setFileName(":/Initialize/RandomActionCards_Norse.txt");
        break;
    case GREEK:
        //actionCardFile.open("E:/School/Spring_2015/CECS_343/AOM_Qt/Initialize/RandomActionCards_Greek.txt");
        //actionCardFile.open(dir::mainDir + "/Initialize/RandomActionCards_Greek.txt");
        actionCardFile.setFileName(":/Initialize/RandomActionCards_Greek.txt");
        break;
    case EGYPTIAN:
        //actionCardFile.open("E:/School/Spring_2015/CECS_343/AOM_Qt/Initialize/RandomActionCards_Egyptian.txt");
        //actionCardFile.open(dir::mainDir + "/Initialize/RandomActionCards_Egyptian.txt");
        actionCardFile.setFileName(":/Initialize/RandomActionCards_Egyptian.txt");
        break;
    default:
        // throw an error???
        break;
    }

    // if no file was opened, then display error message
    if(!actionCardFile.open(QIODevice::ReadOnly)) {
        std::cout << "The Random Action Card input file was not found." << std::endl;
    }

    QList<QByteArray> lineDetails;

    // loop until the file is empty
    //while(actionCardFile >> cardName >> cardRank >> buildNum) {
    while(!actionCardFile.atEnd()) {
        lineDetails = actionCardFile.readLine().split(' ');

        cardName = lineDetails[0].toStdString();
        cardRank = lineDetails[1].toInt();
        buildNum = lineDetails[2].toInt();

        // add a card for each line in the input file
        if(cardName == "TRADE" ||
            cardName == "HERMES" || // Greek
            cardName == "FORSETI" || cardName == "LOKI") { // Norse
            randomDeck.push_back(new TradeCard(cardName, (CardRank)cardRank, this));
        }
        else if(cardName == "GATHER" ||
            cardName == "POSEIDON" || cardName == "HADES" || cardName == "DIONYSUS" || // Greek
            cardName == "RA" || // Egyptian
            cardName == "THOR" || cardName == "SKADI" || cardName == "FREYJA") { // Norse
            //randomDeck.push_back(new GatherCard(cardName, (CardRank)cardRank, this));
        }
        else if(cardName == "NEXT_AGE" ||
            cardName == "ZEUS" || cardName == "HEPHAESTOS" || // Greek
            cardName == "SET" || cardName == "HATHOR" || // Egyptian
            cardName == "ODIN") { // Norse
            randomDeck.push_back(new NextAgeCard(cardName, (CardRank)cardRank, this));
        }
        else if(cardName == "EXPLORE" ||
            cardName == "ARTEMIS" || //Greek
            cardName == "PTAH" || // Egyptian
            cardName == "BALDR") { // Norse
            randomDeck.push_back(new ExploreCard(cardName, (CardRank)cardRank, this));
        }
        else if(cardName == "BUILD" ||
            cardName == "HERA" || // Greek
            cardName == "NEPHTHYS" || cardName == "HORUS" || // Egyptian
            cardName == "NJORD") { // Norse
            //randomDeck.push_back(new BuildCard(cardName, (CardRank)cardRank, buildNum, this));
        }
        else {
            // Card not recognized, display error
            std::cout << cardName << " not recognized." << std::endl;
        }
    } // end while

    // Close the file
    actionCardFile.close();

    // Shuffle the random action card deck
    this->Shuffle();
}

/*
    Performs the action of the card in the player's hand at the indicated index.
    It will return a status number to the display to indicate done or an error.
*/
int player::PerformSelectedAction(int cardIndex) {
   //int status = this->currentHand[cardIndex]->PerformCardAction();
     int status = this->permanentDeck[1]->PerformCardAction();

   return status;
}



/*
    This returns the card size of the current hand. This allows for using
    for loops.
 */
int player::AgeCardSize(){
    return this->currentHand.size();
/*    switch(this->Page){
    case ARCHAIC:
        return 4;
        break;
    case CLASSICAL:
        return 5;
        break;
    case HEROIC:
        return 6;
        break;
    case MYTHIC:
        return 7;
        break;
    } */
}


/*
    This function returns the number of cards in a player's permanent deck.
*/
int player::PermDeckSize() {
    return this->permanentDeck.size();
}


/*
    Determines if a player's hand is full based on their current age. Returns true if the player's
    hand is full and false if it is not full.
    ARCHAIC = 4 Cards
    CLASSICAL = 5 Cards
    HEROIC = 6 Cards
    MYTHIC = 7 Cards
*/
bool player::IsHandFull() {
    switch(this->Page) {
    case ARCHAIC:
        if(this->currentHand.size() >= 4) {
            return true;
        }
        else {
            return false;
        }
        break;
    case CLASSICAL:
        if(this->currentHand.size() >= 5) {
            return true;
        }
        else {
            return false;
        }
        break;
    case HEROIC:
        if(this->currentHand.size() >= 6) {
            return true;
        }
        else {
            return false;
        }
        break;
    case MYTHIC:
        if(this->currentHand.size() >= 7) {
            return true;
        }
        else {
            return false;
        }
        break;
    }

    // Return true by default
    return true;
}


/*
    Shuffles a player's random action card deck.
*/
void player::Shuffle() {
    std::srand((int)std::time(0));
    std::random_shuffle(randomDeck.begin(), randomDeck.end());
}


/*
    Draws the card in the player's permanent action card deck located at the indicated index and
    moves it to their hand. Returns true if the draw was successful and false if it was not.
*/
bool player::DrawPermanent(int cardIndex) {
    // Check to see if their hand is full
    if(this->IsHandFull()) {
        // player's hand is full, cannot draw a card
        return false;
    }

    // Check if the index is valid (error handler?)

    // Move the indicated card to the player's hand and remove it from the deck
    currentHand.push_back(permanentDeck[cardIndex]);
   // permanentDeck.erase(permanentDeck.begin() + cardIndex);

    return true;
}

/*
    Draws a card from the back of the random action card deck and moves it to the player's hand.
    Returns true if the draw was successful and false if it was not.
*/
bool player::DrawRandom() {
    // Check to see if their hand is full
    if(this->IsHandFull()) {
        // player's hand is full, cannot draw a card
        return false;
    }

    // Check if the player's random action card deck is empty, if it is refill from discard pile and shuffle

    // Move card to the player's hand and remove it from the deck
    currentHand.push_back(randomDeck.back());
    randomDeck.pop_back();

    return true;
}


QString player::getCulture(int value){
    QString cult;
	switch (value){
		case 0: //Egyptian
            return cult= "Egyptian";
			break;
        case 1: //Greek
            return cult= "Greek";
			break;
		case 2:
            return cult= "Norse";
			break;
		default:
            return cult= "Heavy";
	}
    return 0;
}

QString player::getBoard(QString boardPart){
    //get cityboard
    if(boardPart == "city")
          return gameboard->getCityArea();
    //get production board
    else if(boardPart == "production")
        return gameboard->getProductionArea();
    //get holding area
    else if(boardPart == "holding")
        return gameboard->getHoldingArea();
    //get whole board
    else
        return gameboard->getCultureBoard();
}


/*
    Function: Assigns the players resources from the bank
 */
void player::setPlayerResources(bank *gameBank){

    int const startingAmt = 4;
    resourceList << "food" << "favor" << "wood" << "victory";
    //Initialize the players resources for the game
    //but first check if bank is empty
    if(gameBank->totalInBank() == 0)
        qDebug() << "Nothing in the bank";
    else{
        //get from the bank

       // gold = gameBank->getGolds(startingAmt); favor = gameBank->getFavors(startingAmt);
       // wood = gameBank->getWoods(startingAmt); villagers = gameBank->getVillagers();
       // victoryPointCubes = gameBank->getVictoryCubes(0); food = gameBank->getFoods(startingAmt);
        AddResc(GOLD,gameBank->getGolds(startingAmt));
        AddResc(FAVOR,gameBank->getFavors(startingAmt));
        AddResc(WOOD,gameBank->getWoods(startingAmt));
        AddResc(FOOD,gameBank->getFoods(startingAmt));
        AddResc(VICTORY,gameBank->getVictoryCubes(0));
    }
    //initialize the resources first follows the production holding
    resourcesProd<<NULL<<NULL<<NULL<<NULL<<NULL<<NULL<<NULL<<NULL
                   <<NULL<<NULL<<NULL<<NULL<<NULL<<NULL<<NULL<<NULL;
    numResourcesProd<<NULL<<NULL<<NULL<<NULL<<NULL<<NULL<<NULL<<NULL
                    <<NULL<<NULL<<NULL<<NULL<<NULL<<NULL<<NULL<<NULL;

}

/*
    Function: Will add resources to the player from the bank
 */
void player::addToResources(bank *gameBank, int amt){

}

/*
	Display the permanent Deck 
*/
std::vector<ActionCard*> player::getPermDeck(){

        return permanentDeck;
        //this does get the address of each object of the correct card class
        //std::list<ActionCard*>::iterator iter;
        //for (iter = permanentDeck.begin(); iter != permanentDeck.end(); ++iter){
        //    (**iter).PerformCardAction(this);
        //}
}

std::string player::getPermCard(int i){

    ActionCard *temp = permanentDeck[i];
    return temp->GetName();
}

std::string player::getCurrentCard(int i){
    ActionCard *temp = currentHand[i];
    return temp->GetName();
}

int player::getCardRank(int i){
    ActionCard *temp = currentHand[i];
    int rank = (int) temp->GetRank();
    return rank;
}


/*
	Display the random deck for information purpose and testing
	so pop and then push back
*/
void player::getRandDeck(){
	//ActionCard *temp;
	//std::list<ActionCard*> templist;
	//while (!randomDeck.empty()){
	//	temp = randomDeck.top();
	//	templist.push_front(temp);
	//	std::cout << ' ' << temp;
	//	randomDeck.pop();
	//}

	//place back into stack
	/* std::list<ActionCard*>::iterator iter;
	for (iter = templist.begin(); iter != templist.end(); ++iter){
		 randomDeck.push(*iter);
	}*/

}

void player::setPermDeck(ActionCard *card){
    //permanentDeck.push_front(card);
}

void player::setRandDeck(ActionCard *card){

    //randomDeck.push(card);
}

void player::setBoard(QString culture){
      //sets up the various board parts
      gameboard->setCultureBoard(culture);
      gameboard->setProductionArea(culture);
}

void player::setProductionTileInfo(int chosenTiles,int productionAreaTiles,QVector<QString> resourceProducing, QVector<QString> NumResource){
    if(productionAreaTiles <16){
        resourcesProd[productionAreaTiles] = resourceProducing[chosenTiles];
        numResourcesProd[productionAreaTiles] = NumResource[chosenTiles];
    }
   // qDebug()<< resourcesProd << numResourcesProd;
}


QVector<QString> player::getProductionArea(){
    return gameboard->getProductionAreaTerrain();
}


/*
     Returns the resource amount that the player currently has for the indicated resource.
     If anything goes wrong, return -1.

     @param rescType The type of resource to be returned.
     @return The amount of resource that the player currently has.
*/
int player::CheckResc(Resource rescType) {
    return this->gameboard->CheckResc(rescType);
}



/*
    Adds the indicated amount of resource to the player's holding area.

    @param rescType The type of resource to be added.
    @param amount The amount of resource to be added.
*/
void player::AddResc(Resource rescType, int amount) {
    this->gameboard->AddResc(rescType, amount);
}


/*
    Removes the indicated amount of resource from the player's holding area. If there is not enough
    resources available, then the remaining is depeted and the actual amount returned is removed.

    @param rescType The type of resource to be removed.
    @param amount The amount of resource to be removed.
    @return The actual amount removed from the holding area.
*/
int player::RemoveResc(Resource rescType, int amount) {
    return this->gameboard->RemoveResc(rescType, amount);
}


