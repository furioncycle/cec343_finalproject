
#include "tiles.h"

using namespace std;
tiles::tiles(){
    //reserve size
    tileHolder.reserve(20);
}
tiles::~tiles(){

}

void tiles::clearTiles(){
    tileHolder.clear();
}

// Function that'll generate random tiles base on user's input
QVector<QString> tiles::randomTile(int user) {
    int count = 0, token;
    int userAmt = user; // Just for now

//  QString const dir = "C:/Users/D.Ly/Dropbox/School Related/CECS 343/Final_Project/cec343_finalproject/soureCodes/AgeOfMeth/pictures/ResourceTiles/";
    QString tempPicDir;

  //  tileHolder;
    srand(time(NULL));
    while (count < userAmt) {
        token = rand() % 20 + 1;
        count++;

        switch (token) {
        case 1: // FERTILE - FOOD
            tempPicDir = dir + "Fertile2Food.png";
            tileHolder.push_back("FERTILE,FOOD,2," + tempPicDir);
            break;
        case 2: // FERTILE - WOOD
            tempPicDir = dir + "Fertile1Wood.png";
            tileHolder.push_back("FERTILE,WOOD,1," + tempPicDir);
            break;
        case 3: // FERTILE - FAVOR
            tempPicDir = dir + "Fertile1Favor.png";
            tileHolder.push_back("FERTILE,FAVOR,1," + tempPicDir);
            break;
        case 4: // FERTILE - GOLD
            tempPicDir = dir + "Fertile1Gold.png";
            tileHolder.push_back("FERTILE,GOLD,1," + tempPicDir);
            break;
        case 5: // FOREST - WOOD
            tempPicDir = dir + "Forest2Wood.png";
            tileHolder.push_back("FOREST,WOOD,2," + tempPicDir);
            break;
        case 6: // FOREST - FOOD
            tempPicDir = dir + "Forest1Food.png";
            tileHolder.push_back("FOREST,FOOD,1," + tempPicDir);
            break;
        case 7: // FOREST - GOLD
            tempPicDir = dir + "Forest1Gold.png";
            tileHolder.push_back("FOREST,GOLD,1," + tempPicDir);
            break;
        case 8: // FOREST - FAVOR
            tempPicDir = dir + "Forest1Favor.png";
            tileHolder.push_back("FOREST,FAVOR,1," + tempPicDir);
            break;
        case 9: // HILL - GOLD
            tempPicDir = dir + "Hills2Gold.png";
            tileHolder.push_back("HILL,GOLD,2," + tempPicDir);
            break;
        case 10: // HILL - FOOD
            tempPicDir = dir + "Hills1Food.png";
            tileHolder.push_back("HILL,FOOD,1," + tempPicDir);
            break;
        case 11: // HILL - WOOD
            tempPicDir = dir + "Hills1Wood.png";
            tileHolder.push_back("HILL,WOOD,1," + tempPicDir);
            break;
        case 12: // HILL - FAVOR
            tempPicDir = dir + "Hills1Favor.png";
            tileHolder.push_back("HILL,FAVOR,1," + tempPicDir);
            break;
        case 13: // MOUNTAIN - GOLD
            tempPicDir = dir + "Mountain2Gold.png";
            tileHolder.push_back("MOUNTAIN,GOLD,2," + tempPicDir);
            break;
        case 14: // MOUNTAIN - WOOD
            tempPicDir = dir + "Mountain1Wood.png";
            tileHolder.push_back("MOUNTAIN,WOOD,1," + tempPicDir);
            break;
        case 15: // MOUNTAIN - FAVOR
            tempPicDir = dir + "Mountain1Favor.png";
            tileHolder.push_back("MOUNTAIN,FAVOR,1," + tempPicDir);
            break;
        case 16: // DESERT - FAVOR
            tempPicDir = dir + "Desert2Favor.png";
            tileHolder.append("DESERT,FAVOR,2," + tempPicDir);
            break;
        case 17: // DESERT - GOLD
            tempPicDir = dir + "Desert1Gold.png";
            tileHolder.push_back("DESERT,WOOD,1," + tempPicDir);
            break;
        case 18: // SWAMP - WOOD
            tempPicDir = dir + "Swamp1Wood.png";
            tileHolder.push_back("SWAMP,WOOD,1," + tempPicDir);
            break;
        case 19: // SWAMP - FOOD
            tempPicDir = dir + "Swamp1Food.png";
            tileHolder.push_back("SWAMP,FOOD,1," + tempPicDir);
            break;
        case 20: // SWAMP - FAVOR
            tempPicDir = dir + "Swamp1Favor.png";
            tileHolder.push_back("SWAMP,FAVOR,1," + tempPicDir);
            break;
        default:
            cout << "ERROR! NO RESOURCE FOUND!" << endl;
            break;
        } // End switch
    }// End while

    //vector<string> temp;

//    for (int i = 0; i < tileHolder.size(); i++) {
//        switch (tileHolder[i]) {
//        case 1:
//            //temp[i] = dir + "";
//            break;
//        default:
//            cout << "ERROR!" << endl;
//            break;
//        }
//    }

    return tileHolder;
}

//enum terrainType { , FOREST, HILL, MOUNTAIN, DESERT, SWAMP };
void resourceTiles::generateResources() {
    QString resourceProduced;

    switch (this->getTerrainType()) { // Functionality is base on terrain type
    case FERTILE:

        break;
    case FOREST:

        break;
    case HILL:

        break;
    case MOUNTAIN:

        break;
    case DESERT:

        break;
    case SWAMP:

        break;
    default:
       qDebug()<< "ERROR! TERRAIN TYPE UNKNOWN!";
        break;
    }// End switch (this->getTerrainType())
}

void buildTiles::specialAbility() {
    switch (this->getBuildType()) { // Functionality is base on build type
    case HOUSE:
        // The player gain one new villager

        //villager++;
        break;
    case WALL:
        /*
        FOR BATTLES!
        Provide a stout layer of defense for a city area, protection for battle...
        */
        break;
    case TOWER:
        /*
        FOR BATTLES!
        Similar to wall but it protect player's prod area...
        */
        break;
    case STOREHOUSE:
        /*
        Reduces the number of resources eliminated due to "Spoilage"
        by allowing the owning player to store up to 8 resources cubes
        of each type from turn to turn instead of the usual 5
        */

        break;
    case MARKET:
        // Reduce costs involved in trading

        //resourceCost = 0;
        break;
    case ARMORY:
        /*
        FOR BATTLES!
        Allows the owner one extra unit in each battle
        */

        break;
    case QUARRY:
        /*
        Reduce the building cost of all future buildings by one resource cube
        Owner get to choose which resource cube.
        */

        break;
    case MONUMENT:
        /*
        Grant the owning player 2 extra favor cubes each time the owning player
        play a Gather card
        */

        // if (gatherCard == true) { favor += 2; )
        break;
    case GRANARY:
        /*
        Grant the owning player 2 extra food cubes each time the owning player
        play a Gather card
        */

        // if (gatherCard == true) { food += 2; )
        break;
    case GOLDMINT:
        /*
        Grant the owning player 2 extra gold cubes each time the owning player
        play a Gather card
        */

        // if (gatherCard == true) { gold += 2; )
        break;
    case WOODWORKSHOP:
        /*
        Grant the owning player 2 extra wood cubes each time the owning player
        play a Gather card
        */

        // if (gatherCard == true) { wood += 2; )
        break;
    case SIEGEENGINEWORKSHOP:
        /*
        FOR BATTLES!
        Dedicated to the destruction of enemy fortifications and structures
        Negates effect of Walls and Towers when player who owns a Siege Engine Workshop
        is attacking. It also allows the owning player to destroy one extra
        building  when successfully attacking another player's city area.
        */
        break;
    case GREATTEMPLE:
        /*
        Allow the owner to purchase one victory point cube for every 8 favor cubes
        To use this ability, the playe rmust play a Trade card. The player
        may purchase as many victory point cubes as they can afford if they have
        enough favor. The trade card may be used normally before purchasing victory point cubes
        */

        /*if (tradeCard == true) {
            if (favor >= 8) {
                victoryPoint++;
                favor -= 8;
            }
        }*/
        break;
    case THEWONDER:
        /*
        When a player build this, the game ends immediately. The player who built
        the wonder gains all victory point cubes that were on the wonder card. A
        player MUST BE in the Mythic Age before the wonder building can be build.
        */

        //if (currentAge == 'MYTHIC') {
        //	// game ends immediately
        //	// player gets all victory point cubes on the wonder card
        //}
        break;
    default:
        qDebug() << "ERROR! BUILD TYPE UNKNOWN!";
        break;
    }
}
