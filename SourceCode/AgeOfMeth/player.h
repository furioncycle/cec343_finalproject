#ifndef __PLAYER_H_INCLUDED
#define __PLAYER_H_INCLUDED

#include <vector>
#include <string>
#include <QString>
#include <QStringList>
#include "ActionCard.h"
#include <QString>
#include "Board.h"
//#include "holdingarea.h"
#include "bank.h"
#include <QDebug>

// Forward Declarations
class ActionCard;

// Directory Namespace
namespace dir {
    //const std::string mainDir = "E:/School/Spring_2015/CECS_343/Git/Git2/cec343_finalproject/soureCodes/AgeOfMeth"; // Josh's computer
    // can change the directory to your own if you want to initialize the player with action cards
    const std::string mainDir = "C:/Users/sir_pirate/Desktop/Class/343/Project/soureCodes/AgeOfMeth";

}

enum culture{
	EGYPTIAN,
	GREEK,
	NORSE,
	LAST
};

 enum age{
	ARCHAIC,
	CLASSICAL,
	HEROIC,
	MYTHIC
};

class player{

public:
	

	//starting 
    player() : currentAge("Archaic"), Page(ARCHAIC) {gameboard = new Board();}
    player(QString name) :playerName(name), currentAge("Archaic"), Page(ARCHAIC) {gameboard = new Board();}
    //player(QString pname, culture pcult) : Pculture(pcult), playerName(pname), currentAge("Archaic"){gameboard = new Board();}
    player(QString playerName, culture playerCulture); // initializes action cards
	~player();

	//setters
    void setCurrentAge(age Page){ this->Page = Page; }
    void setCulture(culture cult){ Pculture = cult; }
	void setCulture(int cult){ Pculture = (culture)cult; }
    void setBoard(QString);
	void setPermDeck(ActionCard *);
	void setRandDeck(ActionCard *);
    void setPlayerResources(bank *);
    void setGold();
    void setWood();
    void setFavor();
    void setFood();
    void setVictory();
    void startingFlag(bool isStarting){startPlayer =isStarting;}
    void setProductionTileInfo(int,int,QVector<QString>, QVector<QString>);

	//getters
    QString getPlayer(){ return playerName; }
    QString getCulture(int);
    QString getCurrentAge();
    QString getBoard(QString);
    std::vector<ActionCard*> getPermDeck();
    std::string getPermCard(int);
    std::string getCurrentCard(int);
	void getRandCard();
    void getPlayerResources(bank *);
    int getResource(int);
    int getFood(){ return food;}
    int getWood(){return wood;}
    int getVictory(){return victoryPointCubes;}
    int getFavor(){return favor;}
    int getGold(){return gold;}
    void addToResources(bank *,int);
    bool getStartFlag(){return startPlayer;}
    QVector<QString> getProductionArea();
    void discard(std::vector<ActionCard*>);
    int AgeCardSize();
    int getCardRank(int);

    // Helpers
    int PerformSelectedAction(int cardIndex);
    void Shuffle();
    bool IsHandFull();
    bool DrawPermanent(int cardIndex);
    bool DrawRandom();
    void InitializeCards();
    age ReturnAge() { return Page;}
    int PermDeckSize();

    // Helpers that control player's resources
    int CheckResc(Resource rescType);
    void AddResc(Resource rescType, int amount);
    int RemoveResc(Resource rescType, int amount);
	//for debug purpose
	void getRandDeck();

private:
	age Page;
	culture Pculture;
	bool startPlayer;
    QString playerName,currentAge;
    int gold,favor,food,wood,villagers,victoryPointCubes;
	std::vector<ActionCard*> randomDeck,permanentDeck, currentHand, discardPile;
    QVector<QString> resourcesProd, numResourcesProd;
    QStringList resourceList;
    Board* gameboard;

	//implemented to test class
	int index;
};
#endif
