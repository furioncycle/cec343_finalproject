/*
	This file contains the class declarations for the action card classes.
*/

#ifndef ACTIONCARD_H
#define ACTIONCARD_H
#include "bank.h"
#include <string>

// Forward Declaration
class player;

// An enumerated list for card ranks
enum CardRank {PERMANENT, RANDOM, GOD};

/*
	The ActionCard class is an abstract class that the various card types will derive from.
*/
class ActionCard {
public:
	// Accessor functons
    void SetName(std::string newName) { name = newName; }
    void SetRank(CardRank newRank) { rank = newRank; }
    void SetOwner(player* newOwner) { owner = newOwner; }
    std::string GetName() {return name; }
    CardRank GetRank() { return rank; }
	// Pure virtual functions
    virtual int PerformCardAction() = 0;
    virtual int PerformPermanentAction() = 0;
    virtual int PerformRandomAction() = 0;
    virtual int PerformGodAction() = 0;
private:
    std::string name;
	CardRank rank; // Permanent, Random, or God
protected:
    player* owner; // Player owning the card
};

/*
	The NextAgeCard class represents the various next age cards that exist in the game.
*/
class NextAgeCard : public ActionCard {
public:
	// Constructors
    NextAgeCard() { SetName("NEXT_AGE"); SetRank(PERMANENT); }
    NextAgeCard(player* cardOwner) { SetName("NEXT_AGE"); SetRank(PERMANENT); SetOwner(cardOwner); }
    NextAgeCard(std::string cardName, CardRank cardRank, player* cardOwner) { SetName(cardName); SetRank(cardRank); SetOwner(cardOwner); }

    virtual int PerformCardAction();
    virtual int PerformPermanentAction();
    virtual int PerformRandomAction();
    virtual int PerformGodAction();
private:
	// none
};

/*
    The GatherCard class represents the various gather cards that exist in the game.
*/
class GatherCard : public ActionCard {
public:
	// Constructors
    GatherCard() { SetName("GATHER"); SetRank(PERMANENT); }
    GatherCard(player* cardOwner) { SetName("GATHER"); SetRank(PERMANENT); SetOwner(cardOwner); }
    GatherCard(std::string cardName, CardRank cardRank, player* cardOwner) { SetName(cardName); SetRank(cardRank); SetOwner(cardOwner); }

    virtual int PerformCardAction();
    virtual int PerformPermanentAction();
    virtual int PerformRandomAction();
    virtual int PerformGodAction();
private:
	// none
};

/*
    The BuildCard class represents the various build cards that exist in the game.
*/
class BuildCard : public ActionCard {
public:
	// Constructors
    BuildCard() { SetName("BUILD"); SetRank(PERMANENT); numBuildings = 1; }
    BuildCard(player* cardOwner) { SetName("BUILD"); SetRank(PERMANENT); numBuildings = 1; SetOwner(cardOwner); }
    BuildCard(std::string cardName, CardRank cardRank, int numBuilds, player* cardOwner) { SetName(cardName); SetRank(cardRank); numBuildings = numBuilds; SetOwner(cardOwner); }

    virtual int PerformCardAction();
    virtual int PerformPermanentAction();
    virtual int PerformRandomAction();
    virtual int PerformGodAction();
private:
    int numBuildings;
};

/*
    The TradeCard class represents the various trade cards that exist in the game.
*/
class TradeCard : public ActionCard {
public:
	// Constructors
    TradeCard() { SetName("TRADE"); SetRank(PERMANENT); }
    TradeCard(player* cardOwner) { SetName("TRADE"); SetRank(PERMANENT); SetOwner(cardOwner); }
    TradeCard(std::string cardName, CardRank cardRank, player* cardOwner) { SetName(cardName); SetRank(cardRank); SetOwner(cardOwner); }
    virtual int PerformCardAction();
    virtual int PerformPermanentAction();
    virtual int PerformRandomAction();
    virtual int PerformGodAction();
private:
	// none
};

/*
    The ExploreCard class represents the various explore cards that exist in the game.
*/
class ExploreCard : public ActionCard {
public:
	// Constructors
    ExploreCard() { SetName("EXPLORE"); SetRank(PERMANENT); }
    ExploreCard(player* cardOwner) { SetName("EXPLORE"); SetRank(PERMANENT); SetOwner(cardOwner); }
    ExploreCard(std::string cardName, CardRank cardRank, player* cardOwner) { SetName(cardName); SetRank(cardRank); SetOwner(cardOwner); }

    virtual int PerformCardAction();
    virtual int PerformPermanentAction();
    virtual int PerformRandomAction();
    virtual int PerformGodAction();
private:
	// none
};

#endif
