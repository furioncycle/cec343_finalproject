#ifndef __BOARD_H_INCLUDED
#define __BOARD_H_INCLUDED

#include <QString>
#include <QVector>
#include "holdingarea.h"

class Board{

public:
    Board();
    ~Board();
    QString getCultureBoard();
    QString getCityArea();
    QString getHoldingArea();
    QString getProductionArea();
    void setCultureBoard(QString);
    void setProductionArea(QString);
    QVector<QString> getProductionAreaTerrain();

    // Helpers that control the holding area
    int CheckResc(Resource rescType);
    void AddResc(Resource rescType, int amount);
    int RemoveResc(Resource rescType, int amount);
private:    

    // pointer to player's holding area
    holdingArea* PlayerHoldingArea;

    QString hold;
    QString ProductionArea;
    QString cityArea;
    QString fullBoard;
    QVector<QString> ProductionAreaTerrain;
    //QString const dir = "C:/Users/sir_pirate/Desktop/Class/343/Project/soureCodes/AgeOfMeth/pictures";
    //QString const dir = "E:/School/Spring_2015/CECS_343/Git/Git2/cec343_finalproject/soureCodes/AgeOfMeth/pictures";
    QString const dir = ":/pictures";
    QString const EgyptBoardGame = dir+"/egyp/egyptian_board_game.png";
    QString const EqyptHoldingArea = dir+"/egyp/Egyptian_Holding_Area_Low_Quality.jpg";
    QString const EqyptProductionArea = dir+"/egyp/egyptian_prod_area.jpg";
    QString const EqyptCityArea = dir+"/egyp/Egyptian_City_Area_Low_Quality.jpg";
    QString const GreekBoardGame = dir+"/Greek/Greek_Game_Board_Low_Quality.jpg";
    QString const GreekHoldingArea = dir+"/Greek/Greek_Holding_Area.jpg";
    QString const GreekProductionArea = dir+"/Greek/Greek_Prod_Area.jpg";
    QString const GreekCityArea = dir+"/Greek/Greek_City_Area.jpg";
    QString const NorseHoldingArea = dir+"/norse/Norse_Holding_Area.jpg";
    QString const NorseProductionArea = dir+"/norse/Norse_Prod_Area.jpg";
    QString const NorseCityArea = dir+"/norse/Norse_City_Area.jpg";
    QString const NorseBoardGame = dir+"/norse/Norse_Game_Board_Low_Quality.jpg";

};
#endif
