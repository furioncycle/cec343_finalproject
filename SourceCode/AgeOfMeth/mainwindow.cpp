#include "mainwindow.h"
#include "ui_mainwindow.h"

//setup mainwindow,players,bank,tiles, and order of players
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),view(new QGraphicsView()),scene(new QGraphicsScene())
{
    ui->setupUi(this);
    user = new player("user");
    ai_1 = new player("ai_one");
    ai_2 = new player("ai_two");
    gameBank = new bank();
    tilesClass = new tiles();
    initialOrderOfPlayers << user << ai_1 << ai_2 << last;
    startingOrderOfPlayers << NULL << NULL << NULL << NULL;
   // startingOrderOfPlayers << user << ai_1 << ai_2;
    resources << 0 << 0 << 0 << 0 << 0;

    //connect the signals that will be emitted throughout the class to its
    //corresponding function
    setupShortcuts();
    connect(this,SIGNAL(nextState()),this,SLOT(stateMachine()));
    connect(this,SIGNAL(startTurn()),this,SLOT(pickResourceProductions()));
    connect(this,SIGNAL(done()),this,SLOT(placeTile()));
    connect(this,SIGNAL(nextPlayer()),this,SLOT(skipPlayer()));

    //initiate the start of the game
    stateFlag = 1;
    emit nextState();


}

MainWindow::~MainWindow()
{
    delete ui;
}

/********************************************************
 *      Function: States
 ********************************************************/
//This goes into the various states of the game and is the focal point
//where after every task it will go hear to either move to or from its
//task
void MainWindow::stateMachine(){
    qDebug()<<"stateMachine: "<<stateFlag;
    switch(stateFlag){
    case 1: //initialize the start of the game

        qDebug()<<"State: ONE";
        title();
    break;
   case 2: //set up starting order of players and cultures

       qDebug()<<"State: TWO";
       initalStart();
    break;
    case 3: //setup user boards and resources

        qDebug()<<"State: THREE";
        setResourcesAndBoard();
    break;
    case 4: //Placing production tiles in there production areas

        qDebug()<<"State: FOUR";
        //setup the production tiles and such
        setupProductionTiles();
        //setup board and grid
        showBoards(user,"production");
        setProductionTileHolder();
        //Start playing rounds
        startingPlayerIndex =0;
        messageBox("Choose your resources tiles carefully");
        gridWindow(directoryList,6);
        emit startTurn();
    break;
    case 5: //Place victory cubes onto the victory cards

        qDebug()<<"State: FIVE";
        //remove production board and show user board again
        removeTileBoard();
        showBoards(user,"");
       //set up victory cards and display
       messageBox("Assign ONE Victory Cube to a victory card for your turn");
       startingPlayerIndex=0;
       displayVictoryCards();
       emit startTurn();
    break;
    case 6: //Set up current hands for all the players

        qDebug()<<"State: SIX";
        startingPlayerIndex=0;
        //Based on Age player selects the amount of cards for there current hand
        //First select from permanent and then after random deck
        //Permanent card can be shown
        acquireCurrentHand();
        //Play action cards
    break;
    case 7: //Player action card hands
        qDebug()<<"State: SEVEN";
        numLoops=0;
        currentTurn();
        //3 rounds of action card play
    break;
    case 8: //Spoilage
        qDebug()<<"State: Eight";
        messageBox("Lets check for spoilage");
        spoilage();
        break;
    default: //if you enter here something is really wrong!
        qDebug()<<"You are lost ";
    }

}

/***************************************************************************************
 *              STATE ONE Functions
 *
 ***************************************************************************************/

/*
       Function: sets up the title screen display with a start button graphic
 */
void MainWindow::title(){
    start = new QPushButton();
    int image_x=1345,image_y=703;
    int button_x =630,button_y=418,button_w=77,button_h=31;
    bool isFlat = true;
    QString buttonString = "Start";
    QString str = dir+"/titleScreen_update2.png";
    setImage(str,image_x,image_y);
    setPushButton(start,buttonString,button_x,button_y,
                  button_w,button_h,isFlat);
    setUpScene();
    connections(1);

}

//Moving on to the next state in the stateMachine function
void MainWindow::afterTitle(){
    stateFlag = 2;
    emit nextState();
}


/***************************************************************************************
 *              STATE TWO Functions
 *
 ***************************************************************************************/

/*
       Function: Starting Game
*/
void MainWindow::initalStart(){
    //set starting players
    initStartPlayer();
    //randomly chose the culture
    getCulture();
}

/*
 *      Function: This randomize the starting players
 *                and puts them into a vector, while
 *                also setting a flag that they are
 *                either starting or not.
 */
void MainWindow::initStartPlayer(){
    int index = 0, random;
    bool check[3],isStarting=true;
    check[0] = false;
    check[1] = false;
    check[2] = false;
    srand(time(NULL));
    //makes sure that the vectors are empty and random
    while (index < numOfPlayers){
        random = (rand() + time(0)) % numOfPlayers;
        if (check[random] == false){
            startingOrderOfPlayers[index++] = initialOrderOfPlayers[random];
            check[random] = true;
        }
    }
    //assigns who is starting based on the first location in the vector
    if(startingOrderOfPlayers[0]->getPlayer() == "user"){
        user->startingFlag(isStarting);
        ai_1->startingFlag(!isStarting);
        ai_2->startingFlag(!isStarting);
    }
    else if(startingOrderOfPlayers[0]->getPlayer() == "ai_two"){
        ai_2->startingFlag(isStarting);
        user->startingFlag(!isStarting);
        ai_1->startingFlag(!isStarting);
    }
    else if(startingOrderOfPlayers[0]->getPlayer()=="ai_one"){
        ai_1->startingFlag(isStarting);
        user->startingFlag(!isStarting);
        ai_2->startingFlag(!isStarting);
    }
}

/*
 *      Function: Get a random culture and display it
 *                this is done by using a vector with
 *                the cultures. While intitalizing the
 *                cultures, we initialize the action card
 *                decks based on the players culture.
 *      Results:
 *                Displaying the assigned culture to the player
 *                of the game
*/
void MainWindow::getCulture(){
    int index = 0, random;
    bool check[3];
    check[0] = false;
    check[1] = false;
    check[2] = false;
    srand(time(NULL));
    while (index < 3){
        random = (rand() + time(0)) % 3;
        if (check[random] == false){
            cultures[index++] = random;
            check[random] = true;
        }
    }


    // assign cultures
    user->setCulture(cultures[0]);
    ai_1->setCulture(cultures[1]);
    ai_2->setCulture(cultures[2]);

    // intialize action card decks
    user->InitializeCards();
    ai_1->InitializeCards();
    ai_2->InitializeCards();

    if(user->getCulture(cultures[0]) == "Egyptian")
            displayCard(Egyptcultcard);
    else if(user->getCulture(cultures[0]) == "Greek")
            displayCard(GreekCultCard);
    else if(user->getCulture(cultures[0])== "Norse")
            displayCard(NorseCultCard);
    else
          displayCard(randmHeavy);

}

/*
 *              Function: This displays the card of the
 *                        culture and its information
 */
void MainWindow::displayCard(QString card){

/************* DISPLAY CARD ******************************/
    //Transparent Window
    QPixmap image(card);
    bool autofill = true, isFlat = true;
    int rgb=160,alpha=178;
    //label values
    popup = new QLabel();
    //label sizes
    int label_x = 550,label_y=120,label_w=270,label_h=420;
    int label_x2 = 450,label_y2=350,label_w2=500,label_h2=300;
    modifyLabel(popup,autofill,rgb,rgb,rgb,alpha);
    labelSize(popup,label_x,label_y,label_w,label_h);
    addImage2Label(popup,image,label_w,label_h);
    setUpScene();
 /****************Continue Button************************/
    //pushbuttons
    cont = new QPushButton();
    QString pushStr = "Continue";
    int push_x=850,push_y=600,push_w=75,push_h=30;
    int push_r=160,push_g=160,push_b=160,push_a=178;
    setPushButton(cont,pushStr,push_x,push_y,push_w,push_h,isFlat);
    modifyPushButton(cont,push_r,push_g,push_b,push_a);
/*****************Card Information**************************/
    //Label with card information
    popup2 = new QLabel();
    QString cardName = getCard();
    //just place a dummy value for the card since this is used for culture only
    QString info = getCardInfo(cardName,3);
    modifyLabel(popup2,false,100,100,100,190);
    labelSize(popup2,label_x2,label_y2,label_w2,label_h2);
    addText2Label(popup2,info,cardName);

    //set up button connection and scene for displaying to user
    setUpScene();
    connections(stateFlag);
    connect(cont,SIGNAL(clicked()),this,SLOT(cultureChosen()));
}

/*******************************************************
 *          Function: Acquire culture card
 *******************************************************/
//Gets the culture name
QString MainWindow::getCard(){
    if(stateFlag == 1){

    }
    else if(stateFlag == 2){ //culture stuff
        if(user->getCulture(cultures[0])== "Egyptian"){
            return "Egyptian";
        }
        else if(user->getCulture(cultures[0]) == "Norse"){
            return "Norse";
        }
        else{
            return "Greek";
        }
    }
}

//Card info based on either culture, permanent, or random deck
//just a little information for the player when they clicked on
//a card
QString MainWindow::getCardInfo(QString card, int rank){
    //check for what card
    QString info,random;
    srand(time(NULL));
    switch(rank){
        case 0: //permanent
            if(card == "EXPLORE"){
                   return info = "         Draw one more tile then the number of players<br><br>"
                                 "Allows you to increase the number of resource<br> "
                                 "producingtiles you own.<br><br><br>NOTE: Once you place a tile down it can not be removed<br>"
                                 "only with a god power or attach.<br><br>P.S: All players interact";
            }
            else if(card == "GATHER"){
                    return info = "Resource Type or Terrain Type: User can only chose one.<br><br>You and only"
                                  " you are allowed before gathering to<br>reposition your villagers and/or"
                                  " battle units. This means<br>also any in the holding area.<br><br>P.S: All players interact";
            }
            else if(card == "BUILD"){
                    return info = "You are allowed to build different structures based on<br> the number given from"
                                  " the card and the amount<br>of resource cubes they have.<br><br>FYI: Can not do"
                                  " anything until user is done and is not a current turn";
            }
            else if(card == "TRADE"){
                    return info = "                   Cost any 2 resource cubes                              <br><br>"
                                  "This allows you to trade resource cubes from your hand to the resrouce cubes<br>"
                                  "of the same amount to the bank. ";
            }
            else if(card == "NEXT_AGE"){
                    return info = "                       Costs                                     <br>"
                                  "           Classical Age = 4 of each resource                     <br>"
                                  "            Heroic Age = 5 of each resource                      <br>"
                                  "            Mythic Age = 6 of each resource                      <br><br>"
                                  "Allows to move up to a different age based on the<br>past age and amount"
                                  "of resources needed. This<br>allows the player togather more cards when"
                                  " drawing<br> for more action cards.";
            }
            break;
        case 1: //Random
            if(card == "EXPLORE"){
                   randomExplore = ((rand()+time(0))% 3)+1;
                   random = QString::number(randomExplore);
                       return info ="Draw "+random+" more tile then the number of players<br><br>"
                             "Allows you to increase the number of resource producing<br> "
                             "tiles you own.<br><br>NOTE: Once you place a tile down it can not be removed<br>"
                             "only with a god power or attach.<br><br>P.S: All players interact";
            }
            else if(card == "GATHER"){
                    return info = "                   All Resources                           <br><br>"
                                  "This allows you to gather resource cubes from all your<br>resource"
                                  " producing tiles. You and only you are allowed<br>before gathering to"
                                  " reposition your villagers<br>and/or battle units.This means also any"
                                  "in the holding area.<br><br>P.S: All players interact";
            }
            else if(card == "BUILD"){
                    return info = "You are allowed to build different structures based on<br>the number given from"
                                  "the card and the amount of resource<br>cubes they have.<br><br>FYI: Can not do"
                                  "anything until user is done and is not a current turn";
            }
            else if(card == "TRADE"){
                    return info = "                   Cost any 1 resource cube                              <br><br>"
                                  "This allows you to trade resource cubes from your hand<br>to the resrouce cubes"
                                  "of the same amount to the bank. ";
            }
            else if(card == "NEXT_AGE"){
                    return info = "                       Costs                                     <br>"
                                  "           Classical Age = 4 of each resource                     <br>"
                                  "            Heroic Age = 5 of each resource                      <br>"
                                  "            Mythic Age = 6 of each resource                      <br><br>"
                                  "Allows to move up to a different age based on the past age<br>and amount"
                                  "of resources needed. This allows the<br>player to gather more cards when"
                                  " drawing for more action cards.";
            }
            break;
        case 2: //God
            if(card == "HORUS"){
                return info = "                         BUILD GOD CARD                               <br>"
                              "           You can Destroy any building from your city area           <br>";
            }
            else if(card == "NEPHTHYS"){
                return info = "                         BUILD GOD CARD                               <br>"
                              "All buildings purchased with this card cost 2 less<br>resroucesthan usual"
                              "(except for houses).";
            }
            else if(card == "PTAH"){
                return info = "                         EXPLORE GOD CARD                             <br>"
                              "     Select two tiles before any other players choose                 <br>";
            }
            else if(card == "RA"){
                return info = "                         GATHER GOD CARD:                             <br>"
                              "                                FOOD                                  <br>"
                              "                      Gather +2 food per food tile";
            }
            else if(card == "SET"){
                return info = "                         NEXT AGE GOD CARD:                            <br>"
                              "          Similar to the other next age cards except:                  <br>"
                              "Trade one of your production tiles with one of the same<br>terrain type"
                              "from another player's board";
            }
            else if(card == "HATHOR"){
                return info = "                         NEXT AGE GOD CARD:                            <br>"
                              "          Similar to the other next age cards except:                  <br>"
                              "Eliminate one food producing tile  from another player's board";
            }
            else if(card == "HERMES" || card == "FORSETI"){
                return info = "                         TRADE GOD CARD:                            <br>"
                              "                        Cost - 0 resources                          <br>"
                              "                 Gain a profit of any 4 resources";
            }
            else if(card =="HERA"){
                return info = "                         BUILD GOD CARD:                          <br><br>"
                              "                         Gain one house";
            }
            else if(card =="ARTEMIS"){
                return info = "                         EXPLORE GOD CARD                         <br><br>"
                              "       Draw 2 more tile then the number of players<br><br>Select 2 tiles"
                              "before any other players choose";
            }
            else if(card =="POSEIDON"){
                return info = "                         GATHER GOD CARD                          <br>"
                              "                     Resoruce Type or Terrain Type            <br><br>"
                              "                     Gain 5 food resources";
            }
            else if(card =="DIONYSUS"){
                return info = "                         GATHER GOD CARD                          <br>"
                              "                            FOOD <br><br>"
                              "        Gather +1 food per food tile.<br>Other players gather nothing";
            }
            else if(card =="HEPHAESTOS"){
                return info = "                         NEXT AGE GOD CARD:                            <br>"
                              "          Similar to the other next age cards except:                  <br>"
                              "    Eliminate one food producing tile (yours or your opponent's";
            }
            else if(card == "LOKI"){
                return info = "                         TRADE GOD CARD:                            <br>"
                              "                        Cost - 0 resources                          <br>"
                              "                 steal 5 resources from any single opponent";
            }
            else if(card == "ODIN"){
                return info = "                         NEXT AGE GOD CARD:                            <br>"
                              "          Similar to the other next age cards except:                  <br>"
                              "    You may play a fourth action for this round after<br> all the players"
                              "played their third card";
            }
            else if(card == "NJORD"){
                return info = "                         BUILD GOD CARD:                          <br>"
                              "                               NJORD<br><br>"
                              "Eliminate any single building owned by  another  player";
            }
            else if(card == "BALDR"){
                return info = "                         EXPLORE GOD CARD                         <br>"
                              "       Draw the same number of tile then the number of players<br><br>Other players"
                              "do not get to select a tile";
            }
            else if(card == "FREYJA"){
                return info = "                         GATHER GOD CARD                          <br>"
                              "                  Resource Type or Terrain Type <br><br>"
                              "                             Gain 5 gold ";
            }
            else if(card == "SKADI"){
                return info = "                         GATHER GOD CARD                          <br>"
                              "                  Resource Type or Terrain Type <br><br>"
                              "                             Others players gather nothing ";
            }
            break;
    }
    if(card == "Egyptian"){
            return info = "Eqyptian Power house with many achievements such<br>"
                       "as surveyingand construction techniques that supported <br>"
                       "the building of monumental pyramids, temples,obelisks;<br> "
                       "a systemof mathematics, a practical and effective <br>"
                       "system of medicine,irrigation systems and agricultural <br>"
                       "production techniques,the first known ships.<br><br> They are ready to play!";

    }
    else if(card == "Norse"){
        return info = "So welcome to the Norse pantheon, which is not just <br>"
                   "Norway but the rest of Scandinavia — which includes <br>"
                   "Denmark and Sweden. Their idea of Heaven was <br>"
                   "VALHALLA. Warriors only. You had to die in battle first <br>"
                   "and be escorted by beautiful blonde VALKYRIES.<br><br>They are VIKINGS"
                   " and are ready to play!";
    }
    else if(card == "Greek"){
       return info = "The Greeks were polytheistic in their religious beliefs.<br>"
                   "Polytheistic means they believed in and worshiped many<br>"
                   "different gods. In Greek mythology, the gods often<br>"
                   "represented different forms of nature. The Greeks<br>"
                   "use there Gods and amazing strength to going to battle<br>"
                   "to see who is the best culture that rules this earth!<br>"
                   "<br><br>They are ready to play!";
    }
}

//Jump to the next state after culture has being set and displayed
void MainWindow::cultureChosen(){
    stateFlag = 3;
    emit nextState();
}

/***************************************************************************************
 *              STATE THREE Functions
 *
 ***************************************************************************************/

/*
 *   Function: set user board and resources for players
 */

void MainWindow::setResourcesAndBoard(){
    int x_boardGame =1345,y_board =703;
    int image_x=100,image_y=600,button_h=80,button_w=80;
    bool isflat = false;
    int image2_x=200;
    ai1 = new QPushButton();
    ai2 = new QPushButton();
    currentHand = new QPushButton();

    //set up players boards
    user->setBoard(user->getCulture(cultures[userLocation]));
    ai_1->setBoard(ai_1->getCulture(cultures[ai1Location]));
    ai_2->setBoard(ai_2->getCulture(cultures[ai2Location]));
    setImage(user->getBoard(""),x_boardGame,y_board);

    //AI buttons
    setPushButton(ai1,NULL,image_x,image_y,button_h,button_w,isflat);
    ai1->setAttribute(Qt::WA_TranslucentBackground);
    ai1->setIcon(QIcon(ai_1->getBoard("")));
    ai1->setIconSize(QSize(button_h,button_w));
    ai1->setToolTip("AI One");
    ai1->raise();
    setPushButton(ai2,NULL,image2_x,image_y,button_h,button_w,isflat);
    ai2->setAttribute(Qt::WA_TranslucentBackground);
    ai2->raise();
    ai2->setIcon(QIcon(ai_2->getBoard("")));
    ai2->setIconSize(QSize(button_h,button_w));
    ai2->setToolTip("AI Two");

    //Current Hand button: set to disabled till current hand is full
    setPushButton(currentHand,NULL,image2_x+100,image_y,button_h,button_w,isflat);
    currentHand->raise();
    currentHand->setToolTip("users current Hand");
    currentHand->setText("Current Hand of Cards");
    currentHand->setEnabled(false);

   //initalize resources view for displaying
    setResourceWidget(user);
    setUpScene();

    //set up bank resources and player resources
    resourcesSetting();

    //signals and next state
    signalMapping(map,ai1);
    signalMapping(map,ai2);
    stateFlag = 4;
    emit nextState();
}

/*
 *   Function: setting resources widget for displaying the users
 *             resoruces on their board.
 */
void MainWindow::setResourceWidget(player *person){
    //Instantiate players resources view
    boarderBox = new QGraphicsRectItem(800,590,490,80);
    titleText = new QGraphicsSimpleTextItem();
    food = new QGraphicsRectItem(810,630,30,30);
    foodText = new QGraphicsSimpleTextItem();
    victory = new QGraphicsRectItem(910,630,30,30);
    victoryText = new QGraphicsSimpleTextItem();
    gold = new QGraphicsRectItem(1010,630,30,30);
    goldText = new QGraphicsSimpleTextItem();
    wood = new QGraphicsRectItem(1110,630,30,30);
    woodText = new QGraphicsSimpleTextItem();
    favor = new QGraphicsRectItem(1210,630,30,30);
    favorText = new QGraphicsSimpleTextItem();

    //Modify and position all the resources and input users info
    QString p = person->getPlayer();
    titleText->setText(p+" Resources"); titleText->setPos(1020,600);
    titleText->setBrush(Qt::white);
    foodText->setText("x"+QString::number(resources[0])); foodText->setBrush(Qt::white);
    foodText->setPos(845,648);
    victoryText->setText("x"+QString::number(resources[1])); victoryText->setBrush(Qt::white);
    victoryText->setPos(945,648);
    goldText->setText("x"+QString::number(resources[2])); goldText->setBrush(Qt::white);
    goldText->setPos(1045,648);
    woodText->setText("x"+QString::number(resources[3])); woodText->setBrush(Qt::white);
    woodText->setPos(1145,648);
    favorText->setText("x"+QString::number(resources[4])); favorText->setBrush(Qt::white);
    favorText->setPos(1245,648);
    boarderBox->setBrush(QColor(67,66,66,178));
    food->setBrush(Qt::green); gold->setBrush(Qt::yellow);
    victory->setBrush(Qt::red); wood->setBrush(QColor(153,76,0,255));
    favor->setBrush(Qt::blue);

    //Add objects to the scene
    scene->addItem(boarderBox); scene->addItem(victory);
    scene->addItem(food); scene->addItem(gold); scene->addItem(wood);
    scene->addItem(favor);scene->addItem(foodText); scene->addItem(victoryText);
    scene->addItem(goldText); scene->addItem(woodText); scene->addItem(favorText);
    scene->addItem(titleText);
}


/*
 *          The mainwindow view
 */
void MainWindow::setUpScene(){
    view->setScene(scene);
    setCentralWidget(view);
}


//Get and display resources for players
//(only user is displayed the start)
void MainWindow::resourcesSetting(){
        messageBox("Intializing resources for game");
        //set Users to there resources
        user->setPlayerResources(gameBank);
        ai_1->setPlayerResources(gameBank);
        ai_2->setPlayerResources(gameBank);
        resources[0] = user->getFood();
        resources[1] = user->getVictory();
        resources[2] = user->getGold();
        resources[3] = user->getWood();
        resources[4] = user->getFavor();
        deleteResourceObj();
        setResourceWidget(user);
}

//update Resource widget on players board
void MainWindow::updateResourceWidget(player* person){
    deleteResourceObj();
    if(person == user){
        resources[0] = user->CheckResc(FOOD);
        resources[1] = user->CheckResc(VICTORY);
        resources[2] = user->CheckResc(FOOD);
        resources[3] = user->CheckResc(WOOD);
        resources[4] = user->CheckResc(FAVOR);
    }
    else if(person == ai_1){
        resources[0] = ai_1->CheckResc(FOOD);
        resources[1] = ai_1->CheckResc(VICTORY);
        resources[2] = ai_1->CheckResc(FOOD);
        resources[3] = ai_1->CheckResc(WOOD);
        resources[4] = ai_1->CheckResc(FAVOR);
    }
    else if(person == ai_2){
        resources[0] = ai_2->CheckResc(FOOD);
        resources[1] = ai_2->CheckResc(VICTORY);
        resources[2] = ai_2->CheckResc(FOOD);
        resources[3] = ai_2->CheckResc(WOOD);
        resources[4] = ai_2->CheckResc(FAVOR);
    }
    setResourceWidget(person);
}

//deallocate pointers for the resourcess for displaying
void MainWindow::deleteResourceObj(){
    delete foodText;delete victoryText; delete favorText;
    delete goldText; delete woodText; delete titleText;
    delete food; delete victory; delete favor;
    delete gold;  delete wood;
}


/***************************************************************************************
 *              STATE FOUR Functions
 *
 ***************************************************************************************/

/*
 *      Function: Production tile setup
 */
void MainWindow::setupProductionTiles(){
    ProdTiles = tilesClass->randomTile(20);
    sortProductionTiles();
}

/*
 *
 *      Function: get production tiles from bank and
 *                seperate into there appropriate lists
 *                for future referencing and placing
 *                various data like resources produced
 *                to the players information.
 */
void MainWindow::sortProductionTiles(){

    //Seperate the string in the various parts
      QVector<QStringList> tempVector;
      //seperate all into a list and then place into a vector
      //taking the text file and split it by the commas
      QStringList tempList;
      for(int i=0;i<ProdTiles.length();i++){
         tempList = ProdTiles[i].split(",");
         tempVector.push_back(tempList);
      }
      //now seperate into there corresponding lists
      for(int j = 0; j<tempVector.length();j++){
          tempList = tempVector[j];
          terrainType.push_back(tempList[0]);
          resourceProduced.push_back(tempList[1]);
          numResourcesProduced.push_back(tempList[2]);
          directoryList.push_back(tempList[3]);
      }
}

/*
*   Function: show Full Board based on player and the area of the board
*/
    void MainWindow::showBoards(player* person,QString b){
    int x_boardGame =1345,y_board =703;
    int image_x=100,image_y=600,button_h=80,button_w=80;
    bool isflat = false;
    int image2_x=200;
    if(person == user){
    //delete ai1,ai2,currentHand;
    setImage(user->getBoard(b),x_boardGame,y_board);
      if(b == ""){ //full board
          ai1 = new QPushButton();
          ai2 = new QPushButton();
          currentHand = new QPushButton();
          //AI buttons
          setPushButton(ai1,NULL,image_x,image_y,button_h,button_w,isflat);
          ai1->setAttribute(Qt::WA_TranslucentBackground);
          ai1->raise();
          ai1->setIcon(QIcon(ai_1->getBoard("")));
          ai1->setIconSize(QSize(button_h,button_w));
          ai1->setToolTip("AI One");
          setPushButton(ai2,NULL,image2_x,image_y,button_h,button_w,isflat);
          ai2->setAttribute(Qt::WA_TranslucentBackground);
          ai2->raise();
          ai2->setIcon(QIcon(ai_2->getBoard("")));
          ai2->setIconSize(QSize(button_h,button_w));
          ai2->setToolTip("AI Two");
          //setup current hand but only enable after drawing action cards
          setPushButton(currentHand,NULL,image2_x+100,image_y,button_h,button_w,isflat);
          currentHand->raise();
          currentHand->setToolTip("users current Hand");
          currentHand->setText("Current Hand of Cards");
          currentHand->setEnabled(false);
          //update resoruces
          updateResourceWidget(user);
          setUpScene();
          //connect the buttons
          signalMapping(map,ai1);
          signalMapping(map,ai2);
          signalMapping(map,currentHand);
      }
    }
    else if(person==ai_1){
        delete ai2,userButton;
        userButton = new QPushButton();
        ai2 = new QPushButton();
        setImage(ai_1->getBoard(""),x_boardGame,y_board);
        //AI buttons
        setPushButton(ai2,NULL,image_x,image_y,button_h,button_w,isflat);
        ai2->setAttribute(Qt::WA_TranslucentBackground);
        ai2->raise();
        ai2->setIcon(QIcon(ai_2->getBoard("")));
        ai2->setIconSize(QSize(button_h,button_w));
        ai2->setToolTip("AI Two");
        setPushButton(userButton,NULL,image2_x,image_y,button_h,button_w,isflat);
        userButton->setAttribute(Qt::WA_TranslucentBackground);
        userButton->raise();
        userButton->setIcon(QIcon(user->getBoard("")));
        userButton->setIconSize(QSize(button_h,button_w));
        userButton->setToolTip("User");
        updateResourceWidget(ai_1);
        //display a warning to user that this is not there
        //board and can not see anything past this display
        setBox(ai_1->getPlayer());
        setUpScene();

        //setup connections
        signalMapping(map,userButton);
        signalMapping(map,ai2);

    }
    else if(person==ai_2){
        delete ai1,userButton;
        userButton = new QPushButton();
        ai1 = new QPushButton();
        setImage(ai_2->getBoard(""),x_boardGame,y_board);

        //AI buttons
        setPushButton(ai1,NULL,image_x,image_y,button_h,button_w,isflat);
        ai1->setAttribute(Qt::WA_TranslucentBackground);
        ai1->raise();
        ai1->setIcon(QIcon(ai_1->getBoard("")));
        ai1->setIconSize(QSize(button_h,button_w));
        ai1->setToolTip("AI One");
        setPushButton(userButton,NULL,image2_x,image_y,button_h,button_w,isflat);
        userButton->setAttribute(Qt::WA_TranslucentBackground);
        userButton->raise();
        userButton->setIcon(QIcon(user->getBoard("")));
        userButton->setIconSize(QSize(button_h,button_w));
        userButton->setToolTip("User");
        updateResourceWidget(ai_2);
        //display a warning to user that this is not there
        //board and can not see anything past this display
        setBox(ai_2->getPlayer());
        setUpScene();

        //setup connections
        signalMapping(map,userButton);
        signalMapping(map,ai1);
    }

}


//Sets up the production areas and its display
//pushbuttons to placed images onto
//This allows for the user to visually see what was placed on to
//his|her board
void MainWindow::setProductionTileHolder(){
    QGridLayout *grid2 = new QGridLayout(view);
    QButtonGroup *group = new QButtonGroup();
    int i =0;
    //make pushButtons
    for(int h = 0; h<16;h++){
        QPushButton *tile2 = new QPushButton();
        tilePlaceHolder.push_back(tile2);
        tile2->setAttribute(Qt::WA_TranslucentBackground);
        //were the random images are placed and a place holder
        tile2->setFlat(true);
        tile2->setFixedSize(300,150);
        group->addButton(tile2);

    }
    //place on grid
    for(int j=0;j<4;j++){
       for(int h = 0; h<4;h++){
            grid2->addWidget(tilePlaceHolder[i],j,h);
            grid2->setAlignment(tilePlaceHolder[i],Qt::AlignTop|Qt::AlignLeft);
            i++;
       }
    }
}

//Displaying the production tiles to chose from in a new window
//that have each button linked to placing them on the players
//board
void MainWindow::gridWindow(QVector<QString> items, int numItems){
    tile.clear();
    tempVect = items;
    displayWindow = new QWidget();
    displayWindow->setWindowTitle("Production Tile");
    grid = new QGridLayout(displayWindow);
    pass = new QPushButton("Pass");
    QButtonGroup *group2 = new QButtonGroup();
    //make the amount of pushbuttons for the card amount
    //set them to flat to look like labels
    int i =0,j,h=0,r=0;
    if(numItems > 10){
        r = 5;
    }
    else{
        r = 2;
    }
    for(int f =0;f<numItems;f++){
        QPushButton *tilePtr = new QPushButton();
        tile.push_back(tilePtr);
        tilePtr->setFlat(true);
        tilePtr->setAttribute(Qt::WA_TranslucentBackground);
        //were the random images come from
        tilePtr->setIcon(QIcon(tempVect[f]));
        tilePtr->setIconSize(QSize(100,200));
        group2->addButton(tilePtr);
    }
    for(int j=0;j<r;j++){
       if(numItems > 10 ){
           for(h = 0; h <4;h++){
                grid->addWidget(tile[i],j,h);
                i++;
           }
       }
       else{
           for(h = 0; h<3;h++){
                grid->addWidget(tile[i],j,h);
                i++;
           }
       }
    }
   grid->addWidget(pass,h+1,h-1);
   if(numItems >10){
       displayWindow->setFixedSize(600,600);
   }
   else{
        displayWindow->setFixedSize(400,400);
   }
  displayWindow->show();
  connect(group2,SIGNAL(buttonClicked(int)),this,SLOT(selectProductionTile(int)));
  if(stateFlag > 6){
      connect(pass,SIGNAL(clicked()),this,SLOT(incPlayer()));
  }
  else{
      connect(pass,SIGNAL(clicked()),this,SLOT(skipPlayer()));
  }
}

//----------------------------------------------SLOTS FOR STATE FOUR----------------------------------------------------

/*
 *
 *         Function: When tiles are clicked on (user only)
 *                   this will display that it has been the
 *                   chosen one and ask if the user wants this
 *                   card. If they do not then it is deselected
 *                   and can chose another one
 */
void MainWindow::selectProductionTile(int id){
    //This little math equation is because the grid buttons
    //send negative values when they are clicked. This
    //makes them to a value that corresponds to vector
    //notation
    TileIdtoLocation = -(id)-2;

    //if they deselect it and didn't want the card
    if(numClicks > 0 && tempID == id){
        tile[TileIdtoLocation]->setStyleSheet("background: transparent");
        tile[TileIdtoLocation]->setFlat(true);
        CardHolder ="";
        numClicks=0;

    }
    else{
        tempID = id;
        numClicks++;
        CardHolder = tempVect[TileIdtoLocation];
        tile[TileIdtoLocation]->setFlat(false);
        tile[TileIdtoLocation]->setStyleSheet("background: yellow");
        isDone = QuestionMessageBox("Is this the card you want?");
        if(isDone == false){
            chosenTileCounter--;
            //deselect that card
            selectProductionTile(tempID);
        }
        else{
            tile[TileIdtoLocation]->setStyleSheet("background: transparent");
            tile[TileIdtoLocation]->setFlat(true);
            tile[TileIdtoLocation]->setEnabled(false);
            chosenTileCounter++;
            emit done();
        }
    }
}

//Skip to the next player for the production tile
/*
 *          This goes for a process of:
 *              1 2 3
 *              3 2 1
 *          looping for 3 times
 */
void MainWindow::skipPlayer(){
    if((flip ==1 && numLoops==3) || chosenTileCounter==6){
            messageBox("Finished placing Tiles");
            displayWindow->hide();
            stateFlag = 5;
            emit nextState();
        }
    else if(startingPlayerIndex<2 && flip==0){
        startingPlayerIndex++;
        pickResourceProductions();
    }
    else if(startingPlayerIndex==2 && flip==0){
        flip=1;
        pickResourceProductions();
    }
    else if(flip==1 && startingPlayerIndex>0){
        startingPlayerIndex--;
        pickResourceProductions();
    }
    else if(flip==1 && startingPlayerIndex == 0 && numLoops<3){
        flip=0; numLoops++;
        pickResourceProductions();
    }
}

// Function: Take picked tile and check if it equals the board terrain and if so store the info
// of everything on the card for the user
void MainWindow::placeTile(){
    int playerCardHolder = TileIdtoLocation;
    int value;
    QString tempCard = CardHolder;
    value = cardEqualTerrain(playerCardHolder,tempCard);
    //store that users information
    user->setProductionTileInfo(playerCardHolder,value,resourceProduced,numResourcesProduced);
    //temporarily store card for future use and saving when
    //switching to other windows of desire
   // tempHolderForProductionTiles[playerCardHolder].append(tempCard);
    displayWindow->show();
    if(stateFlag > 6){
        ExploreCount++;
        incPlayer();
    }
    else{
        emit nextPlayer();
    }

}


//Checks if the card's terrain type is equal to the boards terrain and returns
//that terrain types location on the board to place
int MainWindow::cardEqualTerrain(int cardID,QString card){
    //Users production area strings based on the culture
    displayWindow->hide();
    QVector<QString> boardTerrainNames = user->getProductionArea();
    //loop through the production area to see what matches the card
    for(int i=0; i<16;i++){
        QString temp = boardTerrainNames[i];
        bool equal = (temp == terrainType[cardID]);
        if(i<15){
            if( equal == true && tilePlaceHolder[i]->isEnabled()==true){
                    tilePlaceHolder[i]->setIcon(QIcon(card));
                    tilePlaceHolder[i]->setIconSize(QSize(250,110));
                    tilePlaceHolder[i]->setEnabled(false);
                    return i;

            }
        }
        else{
            if( equal == true && tilePlaceHolder[i]->isEnabled()==true){
                    tilePlaceHolder[i]->setIcon(QIcon(card));
                    tilePlaceHolder[i]->setIconSize(QSize(250,110));
                    tilePlaceHolder[i]->setEnabled(false);
                    return i;
            }
            else{
                messageBox("The card with the terrain Type: "+terrainType[cardID]+" doesn't exist please chose another card");
                displayWindow->show();
                tilePlaceHolder[i]->setEnabled(true);
                tile[cardID]->setEnabled(true);
                if(stateFlag > 6){
                    ExploreCount--;
                }
                else{
                    chosenTileCounter--;
                }
            }
        }
    }
}

//Similar to above except this is to check AI's terrain but not display it
// and makes sure it places the tile in one terrain only
void MainWindow::cardEqualTerrain(int cardID,player *person){
    int cardchosen;
    QVector<QString> boardTerrainNames = person->getProductionArea();
    //loop through the production area to see what matches the card
    for(int i=0; i<16;i++){
        QString temp = boardTerrainNames[i];
        bool equal = (temp == terrainType[cardID]);
        if(i<15){
            if( equal == true && tilePlaceHolder[i]->isEnabled()==true){
                 person->setProductionTileInfo(cardID,i,resourceProduced,numResourcesProduced);
                 break;
            }
        }
        else{
            if( equal == true && tilePlaceHolder[i]->isEnabled()==true){
                  person->setProductionTileInfo(cardID,i,resourceProduced,numResourcesProduced);
                  break;
            }
            else{
                messageBox("The card with the terrain Type: "+terrainType[cardID]+" doesn't exist please chose another card");
                tile[cardID]->setEnabled(true);
                if(stateFlag > 6){
                    ExploreCount--;
                    cardchosen = randAiTile20();
                }
                else{
                    chosenTileCounter--;
                    cardchosen = randAiTile();
                }
                if(cardchosen!=-1){
                   cardEqualTerrain(cardchosen,ai_2);
                }

            }
        }
    }
}

// Function to start the game based on the player
//------if AI randomize choice of production tile
//------if user let them chose
void MainWindow::pickResourceProductions(){
    int randNum,value;
        if(startingOrderOfPlayers[startingPlayerIndex]==ai_1){
            randNum = randAiTile();
            if(randNum !=-1){
                cardEqualTerrain(randNum,ai_1);

            }
            emit nextPlayer();
        }
        else if(startingOrderOfPlayers[startingPlayerIndex] == ai_2){
            randNum = randAiTile();
            if(randNum!=-1){
               cardEqualTerrain(randNum,ai_2);
            }
            emit nextPlayer();

        }
        else if(startingOrderOfPlayers[startingPlayerIndex]== user){

                messageBox("Your turn to chose a card");
        }
}


//Randomize Ai tiless and disable that card
//they have chosen return the location of that
//card to check it against the board terrain
int MainWindow::randAiTile(){
    srand(time(NULL));
    int random = (rand() + time(0)) % 6;
    if(random  < 3){
        messageBox("AI chose to Pass");
        return -1;
    }
    else if(chosenTileCounter==6){
        messageBox("No more cards left to acquire");
        return -1;
    }
    else{
        while(tile[random]->isEnabled() == false){
            random = (rand() + time(0)) % 6;
        }
        tile[random]->setEnabled(false);
        chosenTileCounter++;
        messageBox("AI chose the following GREY OUT card");
        return random;
    }
}

//This will randomize a tile being chosen for the 20 different kind
//and disable and select that tile on the opened grid window.
int MainWindow::randAiTile20(){
    srand(time(NULL));
    int random = (rand() + time(0)) % 20;
    if(random  < 6){
        messageBox("AI chose to Pass");
        return -1;
    }
    else{
        while(tile[random]->isEnabled() == false){
            random = (rand() + time(0)) % 20;
        }
        tile[random]->setEnabled(false);
        messageBox("AI chose the following GREY OUT card");
        return random;
    }
}
/***************************************************************************************
 *              STATE FIVE Functions
 *
 ***************************************************************************************/

//Hides and disables the grid of buttons from the production area
//that was in state four
void MainWindow::removeTileBoard(){
    for(int i=0; i<16;i++){
        tilePlaceHolder[i]->hide();
    }
}

//This shows the tile grids for the production area
//
//Does not delete the icons that are in there but has
//the previous tiles saved unless deleted else where
void MainWindow::showTileBoard(){
    for(int i=0; i<16;i++){
        tilePlaceHolder[i]->show();
    }
}

//Displays a messageBox with a string message
//of your choice
void MainWindow::messageBox(QString msg){

    message = new QMessageBox();
    message->setIcon(QMessageBox::Information);
    message->setText(msg);
    message->exec();
}

/*
 *
 *    Functions: set a vector up and display the victory cards
 *
 */
void MainWindow::displayVictoryCards(){
    int temp;
    victoryCards.push_back(victoryDir+"/wonLastBattle.jpg");
    victoryCards.push_back(victoryDir+"/buildings.jpg");
    victoryCards.push_back(victoryDir+"/largestArmy.jpg");
    victoryCards.push_back(victoryDir+"/wonder.gif");
    gridWindowWithInfo(victoryCards,victoryCards.length());
    displayCards->show();
    displayCards->raise();
    disconnect(this,SIGNAL(done()),this,SLOT(placeTile()));
    disconnect(this,SIGNAL(startTurn()),this,SLOT(pickResourceProductions()));
    disconnect(this,SIGNAL(nextPlayer()),this,SLOT(skipPlayer()));
    connect(this,SIGNAL(startTurn()),this,SLOT(nextTurnForVictoryCards()));
}

//Set up the grid to display the victory cards
//and set up the proper display.
void MainWindow::gridWindowWithInfo(QVector<QString> items,int numItems){
    tempVect = items;
    displayCards = new QWidget();
    displayCards->setStyleSheet("background-color: grey");
    displayCards->setWindowTitle("Victory Cards");
    QGridLayout *gridlay = new QGridLayout(displayCards);
    QButtonGroup *group = new QButtonGroup();
    //Make the amount of push buttons for the card amount
    //set them to flat to look like labels and not buttons
    int i=0,r=numItems/3;
    for(int f=0;f<numItems;f++){
        QPushButton *victoryPtr = new QPushButton();
        victoryCardHolders.push_back(victoryPtr);
        victoryPtr->setFlat(true);
        victoryPtr->setAttribute(Qt::WA_TranslucentBackground);
        //place images on buttons
        victoryPtr->setIcon(QIcon(tempVect[f]));
        victoryPtr->setIconSize(QSize(250,250));
        group->addButton(victoryPtr);
    }
    //add buttons to grid
    for(int j=0;j<4;j++){
        gridlay->addWidget(victoryCardHolders[i],1,j);
        i++;
    }
    victoryCardHolders[0]->setToolTip("Won the last Battle");
    victoryCardHolders[1]->setToolTip("Most Buildings");
    victoryCardHolders[2]->setToolTip("Largest Army");
    victoryCardHolders[3]->setToolTip("The wonder");
    wonder = new QLabel();
    army = new QLabel();
    building = new QLabel();
    winning = new QLabel();
    wonder ->setText("<span style='font-size:18pt; font-weight:600; "
                     "color:#040404;'>x 0</span>");
    wonder->setStyleSheet("qproperty-alignment: AlignTop;");
        wonder->setToolTip("Won the last Battle");
       army->setText("<span style='font-size:18pt; font-weight:600; "
                     "color:#040404;'>x 0</span>");
       army->setStyleSheet("qproperty-alignment: AlignTop;");
       building->setText("<span style='font-size:18pt; font-weight:600; "
                         "color:#040404;'>x 0</span>");
       building->setStyleSheet("qproperty-alignment: AlignTop;");
       winning->setText("<span style='font-size:18pt; font-weight:600; "
                        "color:#040404;'>x 0</span>");
       winning->setStyleSheet("qproperty-alignment: AlignTop;");
       gridlay->addWidget(winning,2,0); gridlay->addWidget(army,2,1);
       gridlay->addWidget(building,2,2); gridlay->addWidget(wonder,2,3);
       QLabel *title = new QLabel();
       title->setText("<span style='font-size:24pt; font-weight:600; "
                      "color:#040404;'>Victory Cards</span>");
       gridlay->addWidget(title,0,0,1,4,Qt::AlignTop | Qt::AlignCenter);
      displayCards->setFixedSize(900,350);
      connect(group,SIGNAL(buttonClicked(int)),this,SLOT(updateGridInfo(int)));
}

//----------------------------------------SLOTS FOR STATE FIVE----------------------------------------------

//FYI: skipPlayer is in SLOTS for state three

//Next Turn for victory card players
void MainWindow::nextTurnForVictoryCards(){
    srand(time(NULL));
    int random = (rand()+time(0)) % 4;
    QString msg;
    //This is for AI to use
    if(random == 0){
        msg = "Chose Won The Last Battle";
    }
    else if(random == 2){
        msg = "Chose Largest Army";
    }
    else if(random == 1){
        msg = "Chose The Most Buildings";
    }
    else if(random == 3){
        msg = "Chose Build The Wonder";
    }
    //pick and add a victory cube at random first need to convert
    //due to group buttons assigning negative values
    random = -(random+2);
    if(startingOrderOfPlayers[startingPlayerIndex]==user){
        messageBox("Pick a victory card ");
    }
    else if(startingOrderOfPlayers[startingPlayerIndex]== ai_1){
        messageBox("AI 1 "+msg);
        updateGridInfo(random);
    }
    else if(startingOrderOfPlayers[startingPlayerIndex]==ai_2){
        messageBox("AI 2 "+msg);
        updateGridInfo(random);
    }
    else{
        messageBox("Everyone finished placing there Victroy Cubes, DRAW YOUR CARDS!");
        displayCards->close();
        stateFlag = 6;
        emit nextState();
    }
}

//updates the gameboard with the corresponding
//victory cubes that were just put down on the board
void MainWindow::updateGridInfo(int id){
    int temp = -(id)-2;
       switch(temp){
           case 0:
               winningVictoryCubes += gameBank->getVictoryCubes(1);
               if(winningVictoryCubes==-1){
                   messageBox("There are not more cubes left in the bank- GAME OVER!!!");
                   //initiate the ending of the game
               }
               else{
                   winning->setText("x "+QString::number(winningVictoryCubes));
                   winning->setStyleSheet("font-size:18pt; font-weight:600;");
                   gameBank->setWinningPointCubes(winningVictoryCubes);
                   startingPlayerIndex++;
               }
           break;
       case 1:
               armyVictoryCubes +=gameBank->getVictoryCubes(1);
               if(armyVictoryCubes==-1){
                   messageBox("There are not more cubes left in the bank- GAME OVER!!!");
                   //initiate the ending of the game
               }
               else{
                   army->setText("x "+QString::number(armyVictoryCubes));
                   army->setStyleSheet("font-size:18pt; font-weight:600;");
                   gameBank->setLargeArmyPointCubes(armyVictoryCubes);
                   startingPlayerIndex++;
               }
            break;
       case 2:
               buildingVictoryCubes += gameBank->getVictoryCubes(1);
               if(buildingVictoryCubes==-1){
                   messageBox("There are not more cubes left in the bank- GAME OVER!!!");
                   //initiate the ending of the game
               }
               else{
                   building->setText("x "+QString::number(buildingVictoryCubes));
                   building->setStyleSheet("font-size:18pt; font-weight:600;");
                   gameBank->setBuildingPointCubes(buildingVictoryCubes);
                   startingPlayerIndex++;
               }
             break;
       case 3:
                wonderVictoryCubes += gameBank->getVictoryCubes(1);
               if(wonderVictoryCubes==-1){
                   messageBox("There are not more cubes left in the bank- GAME OVER!!!");
                   //initiate the ending of the game
               }
               else{
                   wonder->setText("x "+QString::number(wonderVictoryCubes));
                   wonder->setStyleSheet("font-size:18pt; font-weight:600;");
                   gameBank->setWonderPointCubes(wonderVictoryCubes);
                   startingPlayerIndex++;
                }
            break;
       }
       emit startTurn();
}


//This is used for the pushbuttons/shortcuts to switch
//to the various game boards of the players
void MainWindow::switchBoards(int player){
    if(player == 0){ //user
        //for right now just the full board
        showBoards(user,"");
    }
    //For both AI only show the full board
    else if(player==1){
        showBoards(ai_1,"");
    }
    else if(player==2){
        showBoards(ai_2,"");
    }
    else if(player=3){ //user production board
        showBoards(user,"production");
    }
    else if(player=4){ //ai_1 production board
        showBoards(ai_1,"production");
    }
    else if(player=5){ //ai_2 production board
        showBoards(ai_2,"production");
    }
}


/*******************************************************
 *                 STATE SIX
 *
 *******************************************************/
/************************************************************************************************
 *
 *                              Functions: Action card draw
 * **********************************************************************************************/
void MainWindow::acquireCurrentHand(){
    //display which deck to get from
    gridWindowDeckType();
    randAi1Turn();
    randAi2Turn();
}

//display message to chose which deck type to display
void MainWindow::gridWindowDeckType(){
    deckTypeDisplay = new QWidget();
    deckTypeDisplay->setWindowTitle("Deck Types");
    deckTypeDisplay->setStyleSheet("background-color: grey");
    QGridLayout *grid = new QGridLayout(deckTypeDisplay);
    QButtonGroup *group = new QButtonGroup();
    for(int i=0; i<2;i++){
        QPushButton *ptr = new QPushButton();
        ptr->setFlat(false);
        ptr->setAttribute(Qt::WA_TranslucentBackground);
        ptr->setFixedSize(QSize(200,150));
        deckTypeHolder.push_back(ptr);
        group->addButton(ptr);

    }
    //deckTypeHolder[0]->setStyleSheet("font-size:50pt");
    deckTypeHolder[0]->setStyleSheet("font-weight:600");
    deckTypeHolder[0]->setText("Grab from the Permanent Deck");
   // deckTypeHolder[1]->setStyleSheet("font:50pt");
    deckTypeHolder[1]->setStyleSheet("font-weight:600");
    deckTypeHolder[1]->setText("Grab from the Random Deck");
    deckTypeHolder[1]->setEnabled(false);
    deckTypeHolder[1]->setToolTip("Can't click till finished with the Permanent Deck");
    grid->addWidget(deckTypeHolder[0],0,0);
    grid->addWidget(deckTypeHolder[1],0,2);
    QLabel *title = new QLabel();
    title->setText("<span style='font-size:24pt; font-weight:600; color:#000000'>Pick a Deck Type</span>");
    //set randomDeck off till permanent deck is done with
    grid->addWidget(title,3,0,1,3,Qt::AlignTop | Qt::AlignCenter);
    QPushButton *finished = new QPushButton();
    finished->setText("finished");
    grid->addWidget(finished,4,2,1,1,Qt::AlignTop | Qt::AlignCenter);
    deckTypeDisplay->setFixedSize(420,320);
    deckTypeDisplay->show();
    disconnect(group,SIGNAL(buttonClicked(int)),this,SLOT(updateGridInfo(int)));
    connect(group,SIGNAL(buttonClicked(int)),this,SLOT(deckSelected(int)));
    connect(finished,SIGNAL(clicked()),this,SLOT(setUpNextState()));
}

//setup for playing the action cards
void MainWindow::setUpNextState(){
    deckTypeDisplay->hide();
    //setup user board
    stateFlag = 7;
    currentHand->setEnabled(true);
    emit nextState();
}

//do... once the deck type has been selected
//which is either permanent or random deck
void MainWindow::deckSelected(int groupID){

    deckId = -(groupID)-2;
    if(deckId == 0){ //Permanent Deck
        deckTypeDisplay->hide();
        //display perm deck
        displayPermDeck();
        displayPerm->show();
    }
    else if(deckId == 1){ //Random Deck
        //Pick random card from deck
        placeIntoCurrentHand(deckId);
    }
}

// Display the players permanent Deck
void MainWindow::displayPermDeck(){
    int i=0;
    displayPerm = new QWidget();
    displayPerm->setWindowTitle("Permanent Deck");
    displayPerm->setStyleSheet("background-color: grey");
    QGridLayout *grid = new QGridLayout(displayPerm);
    QButtonGroup *group = new QButtonGroup();
    permDone = new QPushButton();
    permDone->setText("Done");

    for(int i=0; i<user->PermDeckSize();i++){ // Need to get size of permanent deck not max hand size
        QPushButton *ptr = new QPushButton();
        ptr->setFlat(true);
        ptr->setAttribute(Qt::WA_TranslucentBackground);
        ptr->setFixedSize(QSize(150,205));
        permHolder.push_back(ptr);
        ptr->setIconSize(QSize(200,200));
        acquirePermCard(ptr,i);
        group->addButton(ptr);
    }

    for(int j=0;j<user->PermDeckSize();j++){ // Need to get size of permanent deck not max hand size
        grid->addWidget(permHolder[i],1,j);
        i++;
    }
    grid->addWidget(permDone,3,4,1,1,Qt::AlignCenter | Qt::AlignTop);
    QLabel *title = new QLabel();
    QLabel *info = new QLabel();
    QString text = setTaskInfo();
    title->setText("<span style='font-size:24pt; font-weight:600; color:#000000'>User's Permanent Deck</span>");
    info->setText(text);
    grid->addWidget(title,0,0,0,0,Qt::AlignCenter | Qt::AlignTop);
    grid->addWidget(info,3,0,1,4,Qt::AlignLeft);
    displayPerm->setFixedSize(850,410);
    connect(group,SIGNAL(buttonClicked(int)),this,SLOT(placeIntoCurrentHand(int)));
    connect(permDone,SIGNAL(clicked()),this,SLOT(pickRandomCards()));
}

//get the permanenet deck based on culture and player
void MainWindow::acquirePermCard(QPushButton *ptr,int i){
    QString temp;
    //gets what type of card it is
   temp = QString::fromStdString(user->getPermCard(i));
   //gets the correct image from the directory
   temp = getActionCardImage(temp,user->getCulture(cultures[0]));
   //sets the image on the pushbutton
   ptr->setIcon(QIcon(temp));
}

//based on the users culture and the action card get the directory
//location of the image and return so to display that image
QString MainWindow::getActionCardImage(QString actionCard, QString culture){
    QString cardImage;
       if(culture == "Egyptian"){
            if(actionCard=="BUILD")
                 cardImage = dir+"/egyp/BUILD_E1.jpg";
            else if(actionCard=="GATHER")
                 cardImage = dir+"/egyp/GATHER_E3.jpg";
            else if(actionCard=="NEXT_AGE")
                 cardImage = dir+"/egyp/NEXT_AGE_E1.jpg";
            else if(actionCard=="TRADE")
                 cardImage = dir+"/egyp/TRADE_E.jpg";
            else if(actionCard=="EXPLORE")
                 cardImage = dir+"/egyp/EXPLORE_E1.jpg";
            else if(actionCard=="HORUS")
                cardImage = dir+"/egyp/BUILD_E2.jpg";
            else if(actionCard=="NEPHTHYS ")
                cardImage = dir+"/egyp/BUILD_E3.jpg";
            else if(actionCard=="PTAH ")
                cardImage = dir+"/egyp/EXPLORE_E2.jpg";
            else if(actionCard=="RA")
                cardImage = dir+"/egyp/GATHER_E2.jpg";
            else if(actionCard=="SET")
                cardImage = dir+"/egyp/NEXT_AGE_E2.jpg";
            else if(actionCard =="HATHOR")
                cardImage = dir+"/egyp/NEXT_AGE_E3.jpg";
       }
       else if(culture == "Greek"){
           if(actionCard=="BUILD")
                cardImage = dir+"/Greek/BUILD_G1.jpg";
           else if(actionCard=="GATHER")
                cardImage = dir+"/Greek/GATHER_G1.jpg";
           else if(actionCard=="NEXT_AGE")
                cardImage = dir+"/Greek/NEXT_AGE_G2";
           else if(actionCard=="TRADE")
                cardImage = dir+"/Greek/TRADE_G1.jpg";
           else if(actionCard=="EXPLORE")
                cardImage = dir+"/Greek/EXPLORE_G1.jpg";
           else if(actionCard=="HERMES ")
               cardImage = dir+"/Greek/TRADE_G2.jpg";
           else if(actionCard=="HERA")
               cardImage = dir+"/Greek/BUILD_G2.jpg";
           else if(actionCard=="ARTEMIS")
               cardImage = dir+"/Greek/ARTEMIS.jpg";
           else if(actionCard=="POSEIDON")
               cardImage = dir+"/Greek/HYDRA.jpg";
           else if(actionCard=="HADES")
               cardImage = dir+"/Greek/HADES.jpg";
           else if(actionCard=="DIONYSUS")
               cardImage = dir+"/Greek/ARES.jpg";
           else if(actionCard=="ZEUS")
                cardImage = dir+"/Greek/NEXT_AGE_G1.jpg";
           else if(actionCard=="HEPHAESTOS")
               cardImage = dir+"/Greek/HEPHAESTUS.jpg";
       }
       else if(culture == "Norse"){
           if(actionCard=="BUILD")
                cardImage = dir+"/norse/BUILD_N1.jpg";
           else if(actionCard=="GATHER")
                cardImage = dir+"/norse/GATHER_N1.jpg";
          else if(actionCard=="NEXT_AGE")
                cardImage = dir+"/norse/NEXT_AGE_N1.jpg";
          else if(actionCard=="TRADE")
                cardImage = dir+"/norse/TRADE_N1.jpg";
          else if(actionCard=="EXPLORE")
                cardImage = dir+"/norse/EXPLORE_N1.jpg";
          else if(actionCard=="FORSETI")
                cardImage = dir+"/norse/TRADE_FORSETI.jpg";
           else if(actionCard=="LOKI")
               cardImage = dir+"/norse/TRADE_LOKI.jpg";
           else if(actionCard=="ODIN")
               cardImage = dir+"/norse/NEXT_AGE_ODIN.jpg";
           else if(actionCard=="NJORD")
               cardImage = dir+"/norse/BUILD_NJORD.jpg";
           else if(actionCard=="BALDR")
               cardImage = dir+"/norse/EXPLORE_BALDR.jpg";
           else if(actionCard=="THOR")
               cardImage = dir+"/norse/GATHER_THOR.jpg";
           else if(actionCard=="FREYJA")
               cardImage = dir+"/norse/GATHER_FREYJA.jpg";
           else if(actionCard=="SKADI")
               cardImage = dir+"/norse/GATHER_SKADI.jpg";
       }
       return cardImage;
}



//Info on deck types based on the state of the game.
//As of right now just for the permanent deck
QString MainWindow::setTaskInfo(){
    QString info;
    if(stateFlag == 6){
        info = "<span style='font-size:18pt; font-weight:400; color:#000000'>Please chose from your "
               "permanent deck to<br> place into your current hand after your are<br> done click on "
               "the done button</span>";
        return info;
    }
    else if(stateFlag == 7){
        info = "<span style='font-size:18pt; font-weight:400; color:#000000'>Please chose the tile and amount"
               "<br>you may pick more than one <br> when done click on "
               "the done button</span>";
        return info;
    }
    else{
        return info = "failed";
    }
}

//based on what the player chose for the deck type
//and what the cardtype is place them into the
//players current hand
void MainWindow::placeIntoCurrentHand(int pushButtonID){
    bool full;
    int actionCardType;
    if(deckId == 0 && user->IsHandFull() == false){
      actionCardType = -(pushButtonID)-2;
      permHolder[actionCardType]->setEnabled(false);
      full =  user->DrawPermanent(actionCardType);
    }
    else if(deckId == 1 && user->IsHandFull()==false){
      full =  user->DrawRandom();
    }
    if(user->IsHandFull()==true){
        if(deckId == 0){
            messageBox("Current Hand is full press continue to move on");
            displayPerm->hide();
            deckTypeDisplay->show();
            deckTypeHolder[1]->setEnabled(false);
            deckTypeHolder[0]->setEnabled(false);
        }
        else{
          messageBox("Current Hand is full press continue to move on");
          //setup user board
        }
    }
}

//This will randomize the AI 1 to gather 2 perm
//the rest will be all random action cards
void MainWindow::randAi1Turn(){
    int index = 0, random;
    bool cardCheck[7],full;
    cardCheck[0] = false;
    cardCheck[1] = false;
    cardCheck[2] = false;
    cardCheck[3] = false;
    cardCheck[4] = false;
    cardCheck[5] = false;
    cardCheck[6] = false;
    srand(time(NULL));
    while(index< ai_1->AgeCardSize()){ //let them chose 2 cards from perm
        random = (rand() + time(0)) % 5;
        if (cardCheck[random] == false){
            ai_1->DrawPermanent(random);
            cardCheck[random] = true;
            index++;
        }
    }
    //get the rest of random cards
    do{
        full = ai_1->DrawRandom();
    }while(ai_1->IsHandFull() == false);
}

//This will randomize the AI 1 to gather 2 perm
//the rest will be all random action cards
void MainWindow::randAi2Turn(){
    int index = 0, random;
    bool cardCheck[7],full;
    cardCheck[0] = false;
    cardCheck[1] = false;
    cardCheck[2] = false;
    cardCheck[3] = false;
    cardCheck[4] = false;
    cardCheck[5] = false;
    cardCheck[6] = false;
    srand(time(NULL));
    while(index<ai_2->AgeCardSize()){ //let them chose 2 cards from perm
        random = (rand() + time(0)) % 5;
        if (cardCheck[random] == false){
            ai_1->DrawPermanent(random);
            cardCheck[random] = true;
            index++;
        }
    }
    //get the rest of random cards
    do{
        full = ai_2->DrawRandom();
    }while(ai_2->IsHandFull() == false);
}

//This will let the user to pick random cards
//they need to click on the button until it says
//they can not pick anymore
void MainWindow::pickRandomCards(){
        displayPerm->hide();
        deckTypeDisplay->show();
        deckTypeHolder[1]->setEnabled(true);
}

/*********************************************************************************************
 *
 *                      STATE SEVEN
 *********************************************************************************************/

//allow to display the current hand of the user
void MainWindow::showCurrentHand(){
    int i=0;
    displayCurrent = new QWidget();
    displayCurrent->setWindowTitle("Current Deck");
    displayCurrent->setStyleSheet("background-color: grey");
    QGridLayout *grid = new QGridLayout(displayCurrent);
    QButtonGroup *group = new QButtonGroup();
    QPushButton *close = new QPushButton();
    close->setText("Close");


    for(int i=0; i<user->AgeCardSize();i++){
        QPushButton *ptr = new QPushButton();
        ptr->setFlat(true);
        ptr->setAttribute(Qt::WA_TranslucentBackground);
        ptr->setFixedSize(QSize(250,250));
        currentHolder.push_back(ptr);
        ptr->setIconSize(QSize(200,200));
        acquireCurrentCard(ptr,i);
        group->addButton(ptr);
    }
    for(int j=0;j<4;j++){
        grid->addWidget(currentHolder[i],1,j);
        i++;
    }
    grid->addWidget(close,3,4,1,1,Qt::AlignCenter | Qt::AlignTop);
    QLabel *title = new QLabel();
    title->setText("<span style='font-size:24pt; font-weight:600; color:#000000'>User's Current Deck</span>");
    grid->addWidget(title,0,0,0,0,Qt::AlignCenter | Qt::AlignTop);
    displayCurrent->setFixedSize(750,400);
    displayCurrent->show();
    connect(group,SIGNAL(buttonClicked(int)),this,SLOT(infoAboutCard(int)));
    connect(close,SIGNAL(clicked()),this,SLOT(closeCurrentHand()));
}

//Get the card image from permanent or random and display on
//the buttons
void MainWindow::acquireCurrentCard(QPushButton *ptr,int i){
    QString temp;
    //gets what type of card it is
   temp = QString::fromStdString(user->getCurrentCard(i));
   //gets the correct image from the directory
   temp = getActionCardImage(temp,user->getCulture(cultures[userLocation]));
   //sets the image on the pushbutton
   ptr->setIcon(QIcon(temp));
}

//hides the users current hand
void MainWindow::closeCurrentHand(){
    displayCurrent->hide();
}
\
//Takes the clicked card and displays it and
//its info
void MainWindow::infoAboutCard(int id){
    int cardHolderId = -(id)-2;
    currentHolder[cardHolderId]->setEnabled(false);
    dispCard(cardHolderId);
}

//similar to the culture section on displaying
//the cards.
//This will display based on what card was selected from
//the group ID.
void MainWindow::dispCard(int cardId){
    currentPlayingActionCard = cardId;
    displayCurrent->hide();
    QString cardType = QString::fromStdString(user->getCurrentCard(currentPlayingActionCard));
    QString cardImage = getActionCardImage(cardType,user->getCulture(cultures[userLocation]));
    cardRank = user->getCardRank(cardId);
    QPixmap image(cardImage);
    bool autofill = true, isFlat = true;
    int rgb=160,alpha=178;
    //label values
    popup = new QLabel();
    //label sizes
    int label_x = 550,label_y=120,label_w=270,label_h=420;
    int label_x2 = 450,label_y2=350,label_w2=500,label_h2=300;
    modifyLabel(popup,autofill,rgb,rgb,rgb,alpha);
    labelSize(popup,label_x,label_y,label_w,label_h);
    addImage2Label(popup,image,label_w,label_h);
    setUpScene();
 /****************Continue Button************************/
    //pushbuttons
    QPushButton *ok = new QPushButton();
    QString pushStr = "OK";
    QPushButton *close = new QPushButton();
    QString closeStr = "Close";
    int push_x=850,push_y=600,push_w=75,push_h=30;
    int push_r=160,push_g=160,push_b=160,push_a=178;

/*****************Card Information**************************/
    //Label with card information
    QLabel *cardInfo = new QLabel();
    QString info = getCardInfo(cardType,cardRank);
    //QGraphicsRectItem *resource = new QGraphicsRectItem(480,590,30,30);
    //setupResourceInfo(resource);
    modifyLabel(cardInfo,false,100,100,100,190);
    labelSize(cardInfo,label_x2,label_y2,label_w2,label_h2);
    addText2Label(cardInfo,info,cardType);
    setPushButton(ok,pushStr,push_x,push_y,push_w,push_h,isFlat);
    modifyPushButton(ok,push_r,push_g,push_b,push_a);

    //setup environment for displaying
    setUpScene();
    connect(ok,SIGNAL(clicked()),this,SLOT(doCardAction()));
}

//display the card with some various message
void MainWindow::dispCard(QString cardType,QString cardImage, QString player, QString msg){
    QPixmap image(cardImage);
    bool autofill = true, isFlat = true;
    int rgb=160,alpha=178;
    //label values
    popup = new QLabel();
    //label sizes
    int label_x = 550,label_y=120,label_w=270,label_h=420;
    int label_x2 = 450,label_y2=350,label_w2=500,label_h2=300;
    modifyLabel(popup,autofill,rgb,rgb,rgb,alpha);
    labelSize(popup,label_x,label_y,label_w,label_h);
    addImage2Label(popup,image,label_w,label_h);
    setUpScene();
 /****************Continue Button************************/
    //pushbuttons
    ok = new QPushButton();
    QString pushStr = "OK";
    int push_x=850,push_y=600,push_w=75,push_h=30;
    int push_r=160,push_g=160,push_b=160,push_a=200;
    setPushButton(ok,pushStr,push_x,push_y,push_w,push_h,isFlat);
    modifyPushButton(ok,push_r,push_g,push_b,push_a);
/*****************Card Information**************************/
    //Label with card information
    cardInfo = new QLabel();
    QString info = msg;
    modifyLabel(cardInfo,false,100,100,100,200);
    labelSize(cardInfo,label_x2,label_y2,label_w2,label_h2);
    addText2Label(cardInfo,info,cardType);
    setUpScene();
    disconnect(ok,SIGNAL(clicked()),this,SLOT(doCardAction()));
    connect(ok,SIGNAL(clicked()),this,SLOT(readStatus()));
}

//This dispCard function is used for AI card display when it is their turn
void MainWindow::dispCard(QString cardType,QString cardImage, QString player){
    QPixmap image(cardImage);
    bool autofill = true, isFlat = true;
    int rgb=160,alpha=178;
    //label values
    popup = new QLabel();
    //label sizes
    int label_x = 550,label_y=120,label_w=270,label_h=420;
    int label_x2 = 450,label_y2=350,label_w2=500,label_h2=300;
    modifyLabel(popup,autofill,rgb,rgb,rgb,alpha);
    labelSize(popup,label_x,label_y,label_w,label_h);
    addImage2Label(popup,image,label_w,label_h);
    setUpScene();
 /****************Continue Button************************/
    //pushbuttons
    ok = new QPushButton();
    QString pushStr = "OK";
    int push_x=850,push_y=600,push_w=75,push_h=30;
    int push_r=160,push_g=160,push_b=160,push_a=200;
    setPushButton(ok,pushStr,push_x,push_y,push_w,push_h,isFlat);
    modifyPushButton(ok,push_r,push_g,push_b,push_a);
/*****************Card Information**************************/
    //Label with card information
    cardInfo = new QLabel();
    QString msg = getCardInfo(cardType,cardRank);
    msg +="<br><br>                    currently Playing: "+player;
    QString info = msg;
    modifyLabel(cardInfo,false,100,100,100,200);
    labelSize(cardInfo,label_x2,label_y2,label_w2,label_h2);
    addText2Label(cardInfo,info,cardType);
    setUpScene();
    connect(ok,SIGNAL(clicked()),this,SLOT(doCardAction()));
}


//This is the order based on who is playing at the time
//
// For user will display current hand and show what they can
//select next for an action
//
// For AI the card is randomly selected from their current
//hand
void MainWindow::whoIsPlaying(){
    if(startingOrderOfPlayers[startingPlayerIndex]==user){
        messageBox("Pick a card from your current hand");
        showCurrentHand();
    }
    else{ //ai
        QString tempCardType,tempCardImage,player; int random;
        //randomize a card based on their age
        srand(time(NULL));
        if(startingOrderOfPlayers[startingPlayerIndex]==ai_1){
            random = (rand()+time(0)) % ai_1->AgeCardSize(); // randomize
           /* do{
                random = (rand()+time(0)) % ai_1->AgeCardSize();
            }while(currentPlayingActionCard == random);*/
            currentPlayingActionCard = random;
            tempCardType = QString::fromStdString(ai_1->getCurrentCard(currentPlayingActionCard));
            cardRank = ai_1->getCardRank(currentPlayingActionCard);
            //gets the correct image from the directory
            tempCardImage = getActionCardImage(tempCardType,ai_1->getCulture(cultures[ai1Location]));
            player = "Ai 1";

        }
        else{
            random = (rand()+time(0)) % ai_1->AgeCardSize(); // randomize
           /* do{
                random = (rand()+time(0)) % ai_2->AgeCardSize();
            }while(currentPlayingActionCard == random);*/
            currentPlayingActionCard = random;
            qDebug()<<"current "<<currentPlayingActionCard;
            tempCardType = QString::fromStdString(ai_2->getCurrentCard(currentPlayingActionCard));
            cardRank = ai_2->getCardRank(currentPlayingActionCard);
            //gets the correct image from the directory
            tempCardImage = getActionCardImage(tempCardType,ai_2->getCulture(cultures[ai2Location]));
            player = "Ai 2";
        }
         dispCard(tempCardType,tempCardImage,player);
    }
}

//See if they played 3 rounds yet
//If they did then move to the next state
void MainWindow::currentTurn(){
    //once finished 3 turns of card play
    if(numLoops==3){
        stateFlag = 8;
        emit nextState();
    }
    else{
        startingPlayerIndex =0;
        numLoops++;
        messageBox("Round: "+QString::number(numLoops));
        whoIsPlaying();
    }
}


//Do the action of the cards
void MainWindow::doCardAction(){
    int status;
    QString tempCardType = QString::fromStdString(startingOrderOfPlayers[startingPlayerIndex]->getCurrentCard(currentPlayingActionCard));
    if(cardRank == 0){ //permanent
        if(tempCardType == "TRADE"){
            //Chose what resource they want to give up
            cost = 1;
            messageBox("which resource will you want to give up ? (chose one)");
            displayResourcesCost(startingOrderOfPlayers[startingPlayerIndex]);
        }
        //works Explore
        else if(tempCardType == "EXPLORE"){
           randomExplore = 1;
             //this will display based on if successful or needing to move to another player with said card
             status = 1;//startingOrderOfPlayers[startingPlayerIndex]->PerformSelectedAction(currentPlayingActionCard);
             if(status == 1){ //can work on action
                messageBox("Playing the Permanent Explore Card: Pick/Pass a resource to place on your board");
                showBoards(user,"production");
                showTileBoard();
                connect(this,SIGNAL(done()),this,SLOT(placeTile()));
                connect(this,SIGNAL(nextPlayer()),this,SLOT(incPlayer()));
                gridWindow(directoryList,20);
                addProductionTiles();
             }
             else{
                 qDebug()<<"Shit went weird in do card action";
             }

        }
        //Nextage down
        else if(tempCardType == "NEXT_AGE"){
             //this will display based on if successful or needing to move to another player with said card
             QString msg, tempCardImage, tempCardType,culture;
             status = startingOrderOfPlayers[startingPlayerIndex]->PerformSelectedAction(currentPlayingActionCard);
             culture = startingOrderOfPlayers[startingPlayerIndex]->getCulture(cultures[startingPlayerIndex]);
             msg = startingOrderOfPlayers[startingPlayerIndex]->getPlayer();
             switch(startingOrderOfPlayers[startingPlayerIndex]->ReturnAge()){
                 case 0: //Archaic
                      msg = msg +" is in the Archaic era still";
                    break;
                 case 1: //Classical
                     msg = msg +" is in the Classic era";
                    break;
                 case 2: //Heroic
                     msg= msg +" is in the Heroic era";
                    break;
                 case 3: //Mythic
                  msg = msg +" is in the Mythic era";
                    break;
             }
             if(status == 1){ //can work on action

                 showBoards(startingOrderOfPlayers[startingPlayerIndex],"");
                //display new age card and info
                tempCardType = QString::fromStdString(startingOrderOfPlayers[startingPlayerIndex]->getCurrentCard(currentPlayingActionCard));
                cardRank = startingOrderOfPlayers[startingPlayerIndex]->getCardRank(currentPlayingActionCard);

                 //gets the correct image from the directory
                tempCardImage = getActionCardImage(tempCardType,culture);
                dispCard(tempCardType,tempCardImage,startingOrderOfPlayers[startingPlayerIndex]->getPlayer(),msg);

             }
             else{
                 messageBox("Couldn't increment to next age not enough resources");
                 //display new age card and info
                 showBoards(startingOrderOfPlayers[startingPlayerIndex],"");
                 tempCardType = QString::fromStdString(startingOrderOfPlayers[startingPlayerIndex]->getCurrentCard(currentPlayingActionCard));
                 cardRank = startingOrderOfPlayers[startingPlayerIndex]->getCardRank(currentPlayingActionCard);
                  //gets the correct image from the directory
                 tempCardImage = getActionCardImage(tempCardType,culture);
                 msg = msg + " still";
                 dispCard(tempCardType,tempCardImage,startingOrderOfPlayers[startingPlayerIndex]->getPlayer(),msg);
             }
        }
    }
    else if(cardRank==1){ //random
        if(tempCardType == "EXPLORE" || tempCardType =="PTAH" || tempCardType == "ARTEMIS"){
             //this will display based on if successful or needing to move to another player with said card
             status = 1;//startingOrderOfPlayers[startingPlayerIndex]->PerformSelectedAction(currentPlayingActionCard);
             if(status == 1){ //can work on action
                messageBox("Playing the Permanent Explore Card: Pick/Pass a resource to place on your board");
                showBoards(user,"production");
                showTileBoard();
                connect(this,SIGNAL(done()),this,SLOT(placeTile()));
                connect(this,SIGNAL(nextPlayer()),this,SLOT(incPlayer()));
                gridWindow(directoryList,20);
                addProductionTiles();
             }
             else{
                 qDebug()<<"Shit went weird in do card action";
             }

        }
        else if(tempCardType == "NEXT_AGE"){
             //this will display based on if successful or needing to move to another player with said card
             QString msg, tempCardImage, tempCardType,culture;
             status = startingOrderOfPlayers[startingPlayerIndex]->PerformSelectedAction(currentPlayingActionCard);
             culture = startingOrderOfPlayers[startingPlayerIndex]->getCulture(cultures[startingPlayerIndex]);
             msg = startingOrderOfPlayers[startingPlayerIndex]->getPlayer();
             switch(startingOrderOfPlayers[startingPlayerIndex]->ReturnAge()){
                 case 0: //Archaic
                      msg = msg +" is in the Archaic era still";
                    break;
                 case 1: //Classical
                     msg = msg +" is in the Classic era";
                    break;
                 case 2: //Heroic
                     msg= msg +" is in the Heroic era";
                    break;
                 case 3: //Mythic
                  msg = msg +" is in the Mythic era";
                    break;
             }
             if(status == 1){ //can work on action

                 showBoards(startingOrderOfPlayers[startingPlayerIndex],"");
                //display new age card and info
                tempCardType = QString::fromStdString(startingOrderOfPlayers[startingPlayerIndex]->getCurrentCard(currentPlayingActionCard));
                cardRank = startingOrderOfPlayers[startingPlayerIndex]->getCardRank(currentPlayingActionCard);

                 //gets the correct image from the directory
                tempCardImage = getActionCardImage(tempCardType,culture);
                dispCard(tempCardType,tempCardImage,startingOrderOfPlayers[startingPlayerIndex]->getPlayer(),msg);

             }
             else{
                 qDebug()<<"Couldn't increment to next age not enough resources";
                 //display new age card and info
                 showBoards(startingOrderOfPlayers[startingPlayerIndex],"");
                 tempCardType = QString::fromStdString(startingOrderOfPlayers[startingPlayerIndex]->getCurrentCard(currentPlayingActionCard));
                 cardRank = startingOrderOfPlayers[startingPlayerIndex]->getCardRank(currentPlayingActionCard);
                  //gets the correct image from the directory
                 tempCardImage = getActionCardImage(tempCardType,culture);
                 msg = msg + " still";
                 dispCard(tempCardType,tempCardImage,startingOrderOfPlayers[startingPlayerIndex]->getPlayer(),msg);
             }
        }
    }

   /* else if(tempCardType == "BUILD"){
         status = startingOrderOfPlayers[startingPlayerIndex]->PerformSelectedAction(currentPlayingActionCard);
    }
    else if(tempCardType == "GATHER"){
         status = startingOrderOfPlayers[startingPlayerIndex]->PerformSelectedAction(currentPlayingActionCard);
    }
    else if(tempCardType == "TRADE"){
         status = startingOrderOfPlayers[startingPlayerIndex]->PerformSelectedAction(currentPlayingActionCard);
    }

    else if(tempCardType == "NEXT_AGE"){

    }*/

}

void MainWindow::addProductionTiles(){
    int randNum;
    if(startingOrderOfPlayers[startingPlayerIndex]==user){
        messageBox("Pick a resource");
    }
    else if(startingOrderOfPlayers[startingPlayerIndex]==ai_1){
        randNum = randAiTile20();
        if(randNum!=-1){
           cardEqualTerrain(randNum,ai_1);
        }
        ExploreCount++;
        incPlayer();
    }
    else if(startingOrderOfPlayers[startingPlayerIndex]==ai_2){
        randNum = randAiTile20();
        if(randNum!=-1){
           cardEqualTerrain(randNum,ai_2);
        }
        ExploreCount++;
        incPlayer();
    }

}

//slot for signal nextPlayer
void MainWindow::incPlayer(){
    //if we finished going through the last player's amount of tiles
    //chosen go to the next round of cards
    if(startingPlayerIndex==2 && (ExploreCount == (randomExplore +3))){
        //display current hand again and let them chose what to play next
        qDebug()<<"Now you can set up the next step";
        displayWindow->hide();
        showBoards(user,"");
        removeTileBoard();
        currentTurn();
    }
    //If we are still not at the last player but we just finished the
    //current players tile selection then set to next player and
    //reset count then get player tiles.
    else if( startingPlayerIndex < 2 && (randomExplore +3) == ExploreCount){
        ExploreCount =0;
        startingPlayerIndex++;
        addProductionTiles();
    }
    //if player is not last player and still in its turn
    else{
        addProductionTiles();
    }
}

void MainWindow::readStatus(){
    currentTurn();
}


//Display window to pick resourcces
//First time you chose the resource you are giving up
//Second time you are chosing the ones you want to trade
//Thrid time you are chosing the one to add to.
void MainWindow::displayResourcesCost(player* p){
    int i=0;
    dispResource = new QWidget();
    dispResource->setWindowTitle("Players Resources");
    dispResource->setStyleSheet("background-color: grey");
    QGridLayout *gridCost = new QGridLayout(dispResource);
    groupCost = new QButtonGroup();
    for(int i=0; i<5;i++){
        QPushButton *ptrCost = new QPushButton();
        ptrCost->setFlat(false);
        ptrCost->setFixedSize(QSize(150,150));
        resourceHolder.push_back(ptrCost);
        groupCost->addButton(ptrCost);
    }
    //set the colors of the ptr
    resourceHolder[0]->setStyleSheet("QPushButton {background-color: red;}");
    resourceHolder[1]->setStyleSheet("QPushButton {background-color: blue;}");
    resourceHolder[2]->setStyleSheet("QPushButton {background-color: green;}");
    resourceHolder[3]->setStyleSheet("QPushButton {background-color: yellow;}");
    resourceHolder[4]->setStyleSheet("QPushButton {background-color: brown;}");
    for(int j=0;j<5;j++){
        gridCost->addWidget(resourceHolder[i],1,j);
        i++;
    }
    QLabel *title = new QLabel();  foodT = new QLabel();
    woodT = new QLabel(); QLabel *info = new QLabel();
    victT = new QLabel();  goldT = new QLabel();
    favorT = new QLabel();
    title->setText("<span style='font-size:24pt; font-weight:600; color:#000000'>Player's Resources</span>");
    victT->setText("<span style='font-size:9pt; font-weight:600; color:#000000'>Victory Cubes:  </span>"+
                   QString::number(p->CheckResc(VICTORY)));
    foodT->setText("<span style='font-size:9pt; font-weight:600; color:#000000'>Food:  </span>"+
                   QString::number(p->CheckResc(FOOD)));
    woodT->setText("<span style='font-size:9pt; font-weight:600; color:#000000'>Wood:  </span>"+
                   QString::number(p->CheckResc(WOOD)));
    goldT->setText("<span style='font-size:9pt; font-weight:600; color:#000000'>Gold:  </span>"+
                   QString::number(p->CheckResc(GOLD)));
    favorT->setText("<span style='font-size:9pt; font-weight:600; color:#000000'>Favor:  </span>"+
                    QString::number(p->CheckResc(FAVOR)));
    gridCost->addWidget(title,0,0,0,0,Qt::AlignCenter | Qt::AlignTop);
    gridCost->addWidget(victT,2,0,1,1,Qt::AlignLeft);
    gridCost->addWidget(favorT,2,1,1,1,Qt::AlignLeft);
    gridCost->addWidget(foodT,2,2,1,1,Qt::AlignLeft);
    gridCost->addWidget(goldT,2,3,1,1,Qt::AlignLeft);
    gridCost->addWidget(woodT,2,4,1,1,Qt::AlignLeft);

    dispResource->setFixedSize(850,410);
    dispResource->show();
    connect(groupCost,SIGNAL(buttonClicked(int)),this,SLOT(takeResource(int)));
}

void MainWindow::displayResourceTrade(player* p){
    int i=0;
    dispResource = new QWidget();
    dispResource->setWindowTitle("Players Resources");
    dispResource->setStyleSheet("background-color: grey");
    QGridLayout *gridTrade = new QGridLayout(dispResource);
    groupTrade = new QButtonGroup();
    doneTrade = new QPushButton();
    doneTrade->setText("Finished");
    for(int i=0; i<5;i++){
        QPushButton *ptrTrade = new QPushButton();
        ptrTrade->setFlat(false);
        ptrTrade->setFixedSize(QSize(150,150));
        resourceHolder.push_back(ptrTrade);
        groupTrade->addButton(ptrTrade);
    }
    //set the colors of the ptr
    resourceHolder[0]->setStyleSheet("QPushButton {background-color: red;}");
    resourceHolder[1]->setStyleSheet("QPushButton {background-color: blue;}");
    resourceHolder[2]->setStyleSheet("QPushButton {background-color: green;}");
    resourceHolder[3]->setStyleSheet("QPushButton {background-color: yellow;}");
    resourceHolder[4]->setStyleSheet("QPushButton {background-color: brown;}");
    for(int j=0;j<5;j++){
        gridTrade->addWidget(resourceHolder[i],1,j);
        i++;
    }
    QLabel *title = new QLabel();  foodT = new QLabel();
    woodT = new QLabel(); QLabel *info = new QLabel();
    victT = new QLabel();  goldT = new QLabel();
    favorT = new QLabel();
    QString text = setTaskInfo();
    info->setText(text);
    title->setText("<span style='font-size:24pt; font-weight:600; color:#000000'>Player's Resources</span>");
    victT->setText("<span style='font-size:9pt; font-weight:600; color:#000000'>Victory Cubes:  </span>"+
                   QString::number(p->CheckResc(VICTORY)));
    foodT->setText("<span style='font-size:9pt; font-weight:600; color:#000000'>Food:  </span>"+
                   QString::number(p->CheckResc(FOOD)));
    woodT->setText("<span style='font-size:9pt; font-weight:600; color:#000000'>Wood:  </span>"+
                   QString::number(p->CheckResc(WOOD)));
    goldT->setText("<span style='font-size:9pt; font-weight:600; color:#000000'>Gold:  </span>"+
                   QString::number(p->CheckResc(GOLD)));
    favorT->setText("<span style='font-size:9pt; font-weight:600; color:#000000'>Favor:  </span>"+
                    QString::number(p->CheckResc(FAVOR)));
    gridTrade->addWidget(info,3,0,1,4,Qt::AlignLeft);
    gridTrade->addWidget(doneTrade,3,4,1,5,Qt::AlignLeft);
    gridTrade->addWidget(title,0,0,0,0,Qt::AlignCenter | Qt::AlignTop);
    gridTrade->addWidget(victT,2,0,1,1,Qt::AlignLeft);
    gridTrade->addWidget(favorT,2,1,1,1,Qt::AlignLeft);
    gridTrade->addWidget(foodT,2,2,1,1,Qt::AlignLeft);
    gridTrade->addWidget(goldT,2,3,1,1,Qt::AlignLeft);
    gridTrade->addWidget(woodT,2,4,1,1,Qt::AlignLeft);
    dispResource->setFixedSize(850,410);
    dispResource->show();
   disconnect(groupCost,SIGNAL(buttonClicked(int)),this,SLOT(takeResource(int)));
   connect(groupTrade,SIGNAL(buttonClicked(int)),this,SLOT(ResourceTrade(int)));
   connect(doneTrade,SIGNAL(clicked()),this,SLOT(updateWantedResources()));
}

void MainWindow::displayResourceUpdated(player* p){
    int i=0;
    dispResource = new QWidget();
    dispResource->setWindowTitle("Players Resources");
    dispResource->setStyleSheet("background-color: grey");
    QGridLayout *gridUpdated = new QGridLayout(dispResource);
    groupUpdated = new QButtonGroup();
    doneUpdated = new QPushButton();
    doneUpdated->setText("Finished");
    for(int i=0; i<5;i++){
        QPushButton *ptrUpdated = new QPushButton();
        ptrUpdated->setFlat(false);
        ptrUpdated->setFixedSize(QSize(150,150));
        resourceHolder.push_back(ptrUpdated);
        groupUpdated->addButton(ptrUpdated);
    }
    //set the colors of the ptr
    resourceHolder[0]->setStyleSheet("QPushButton {background-color: red;}");
    resourceHolder[1]->setStyleSheet("QPushButton {background-color: blue;}");
    resourceHolder[2]->setStyleSheet("QPushButton {background-color: green;}");
    resourceHolder[3]->setStyleSheet("QPushButton {background-color: yellow;}");
    resourceHolder[4]->setStyleSheet("QPushButton {background-color: brown;}");
    for(int j=0;j<5;j++){
        gridUpdated->addWidget(resourceHolder[i],1,j);
        i++;
    }
    QLabel *title = new QLabel();  foodT = new QLabel();
    woodT = new QLabel(); QLabel *info = new QLabel();
    victT = new QLabel();  goldT = new QLabel();
    favorT = new QLabel();
    QString text = "<span style='font-size:24pt; font-weight:600; color:#000000'>Pick a resource you want to update</span>";
    info->setText(text);
    title->setText("<span style='font-size:24pt; font-weight:600; color:#000000'>Player's Resources</span>");
    victT->setText("<span style='font-size:9pt; font-weight:600; color:#000000'>Victory Cubes:  </span>"+
                   QString::number(p->CheckResc(VICTORY)));
    foodT->setText("<span style='font-size:9pt; font-weight:600; color:#000000'>Food:  </span>"+
                   QString::number(p->CheckResc(FOOD)));
    woodT->setText("<span style='font-size:9pt; font-weight:600; color:#000000'>Wood:  </span>"+
                   QString::number(p->CheckResc(WOOD)));
    goldT->setText("<span style='font-size:9pt; font-weight:600; color:#000000'>Gold:  </span>"+
                   QString::number(p->CheckResc(GOLD)));
    favorT->setText("<span style='font-size:9pt; font-weight:600; color:#000000'>Favor:  </span>"+
                    QString::number(p->CheckResc(FAVOR)));
    gridUpdated->addWidget(info,3,0,1,4,Qt::AlignLeft);
    gridUpdated->addWidget(doneUpdated,3,4,1,5,Qt::AlignLeft);
    gridUpdated->addWidget(title,0,0,0,0,Qt::AlignCenter | Qt::AlignTop);
    gridUpdated->addWidget(victT,2,0,1,1,Qt::AlignLeft);
    gridUpdated->addWidget(favorT,2,1,1,1,Qt::AlignLeft);
    gridUpdated->addWidget(foodT,2,2,1,1,Qt::AlignLeft);
    gridUpdated->addWidget(goldT,2,3,1,1,Qt::AlignLeft);
    gridUpdated->addWidget(woodT,2,4,1,1,Qt::AlignLeft);

    dispResource->setFixedSize(850,410);
    dispResource->show();
    disconnect(groupTrade,SIGNAL(buttonClicked(int)),this,SLOT(ResourceTrade(int)));
    disconnect(doneTrade,SIGNAL(clicked()),this,SLOT(updateWantedResources()));
    connect(groupUpdated,SIGNAL(buttonClicked(int)),this,SLOT(add2Resources(int)));
    connect(doneUpdated,SIGNAL(clicked()),this,SLOT(finshedAction()));
}

//After you finished the trade action see if last turn or not
void MainWindow::finshedAction(){
    messageBox("Resources has been trade on to the next action");
    dispResource->close();
    currentTurn();
}


//Add the total traded to the chosen resource
void MainWindow::add2Resources(int id){
    qDebug()<<totalAmt;
    dispResource->hide();
    int location = -(id)-2;
    switch(location){
        case 0: //victory
                startingOrderOfPlayers[startingPlayerIndex]->AddResc(VICTORY,totalAmt);
            break;
        case 1: //favor
               startingOrderOfPlayers[startingPlayerIndex]->AddResc(FAVOR,totalAmt);
            break;
        case 2: //food
            startingOrderOfPlayers[startingPlayerIndex]->AddResc(FOOD,totalAmt);
            break;
        case 3: //gold
            startingOrderOfPlayers[startingPlayerIndex]->AddResc(GOLD,totalAmt);
            break;
        case 4: //wood
            startingOrderOfPlayers[startingPlayerIndex]->AddResc(WOOD,totalAmt);
            break;
    }
    messageBox("Finsihed adding the resource");
    showBoards(startingOrderOfPlayers[startingPlayerIndex],"");
}





//Get the total picked resources and update them and add
//them all together. For the next two functions


//This function houses what was clicked
void MainWindow::ResourceTrade(int id){
    qDebug()<<"went in here resource trade";
    int value = -(id)-2;
    switch(value){
        case 0: //victory
                pickedResources.push_back(0);
            break;
        case 1: //favor
                pickedResources.push_back(1);
            break;
        case 2: //food
            pickedResources.push_back(2);
            break;
        case 3: //gold
            pickedResources.push_back(3);
            break;
        case 4: //wood
            pickedResources.push_back(4);
            break;
    }
}

//This updates said resources
void MainWindow::updateWantedResources(){
    //look to see how many of each where clicked
    qDebug()<<pickedResources.length();
    dispResource->hide();
    int wood=0,food=0,gold=0,victory=0,favor=0;
    for(int i=0; i<pickedResources.length();i++){
        int value = pickedResources[i];
        if(value == 2){

            if(food > startingOrderOfPlayers[startingPlayerIndex]->CheckResc(FOOD)){
            }
            else{
                 food++;
            }

        }
        else if(value== 3){
            if(gold > startingOrderOfPlayers[startingPlayerIndex]->CheckResc(GOLD)){
            }
            else{
                 gold++;
            }
        }
        else if(value== 4){
            if(wood > startingOrderOfPlayers[startingPlayerIndex]->CheckResc(WOOD)){
            }
            else{
                 wood++;
            }
        }
        else if(value== 0){
            if(victory > startingOrderOfPlayers[startingPlayerIndex]->CheckResc(VICTORY)){
            }
            else{
                 victory++;
            }
        }
        else if(value== 1){
            if(favor > startingOrderOfPlayers[startingPlayerIndex]->CheckResc(FAVOR)){
            }
            else{
                 favor++;
            }
        }
    }
    totalAmt = wood+food+gold+victory+favor;
    qDebug()<<victory<<favor<<food<<gold<<wood;
    //update the removal and show
    startingOrderOfPlayers[startingPlayerIndex]->RemoveResc(WOOD,wood);
    startingOrderOfPlayers[startingPlayerIndex]->RemoveResc(FOOD,food);
    startingOrderOfPlayers[startingPlayerIndex]->RemoveResc(GOLD,gold);
    startingOrderOfPlayers[startingPlayerIndex]->RemoveResc(VICTORY,victory);
    startingOrderOfPlayers[startingPlayerIndex]->RemoveResc(FAVOR,favor);
    showBoards(startingOrderOfPlayers[startingPlayerIndex],"");
    displayResourceUpdated(startingOrderOfPlayers[startingPlayerIndex]);

}


//Take the given resource they want as a cost
//and check if they can afford it
void MainWindow::takeResource(int id){
   int location = -(id)-2;
   int resourceAmt;
   dispResource->hide();
   switch(location){
   case 0: //victory
      resourceAmt=startingOrderOfPlayers[startingPlayerIndex]->CheckResc(VICTORY);
       break;
   case 1: //favor
      resourceAmt= startingOrderOfPlayers[startingPlayerIndex]->CheckResc(FAVOR);
       break;
   case 2: //food
       resourceAmt=startingOrderOfPlayers[startingPlayerIndex]->CheckResc(FOOD);
       break;
   case 3: //gold
       resourceAmt=startingOrderOfPlayers[startingPlayerIndex]->CheckResc(GOLD);
       break;
   case 4: //wood
      resourceAmt= startingOrderOfPlayers[startingPlayerIndex]->CheckResc(WOOD);
       break;
   }
   if(resourceAmt < cost){
           messageBox("You waste a turn how sad =[");
           currentTurn();
   }
   else{
       messageBox("taking "+QString::number(cost));
       switch(location){
       case 0: //victory
          resourceAmt=startingOrderOfPlayers[startingPlayerIndex]->RemoveResc(VICTORY,cost);
           break;
       case 1: //favor
          resourceAmt= startingOrderOfPlayers[startingPlayerIndex]->RemoveResc(FAVOR,cost);
           break;
       case 2: //food
           resourceAmt=startingOrderOfPlayers[startingPlayerIndex]->RemoveResc(FOOD,cost);
           break;
       case 3: //gold
           resourceAmt=startingOrderOfPlayers[startingPlayerIndex]->RemoveResc(GOLD,cost);
           break;
       case 4: //wood
          resourceAmt= startingOrderOfPlayers[startingPlayerIndex]->RemoveResc(WOOD,cost);
           break;
       }
       showBoards(startingOrderOfPlayers[startingPlayerIndex],"");
       displayResourceTrade(startingOrderOfPlayers[startingPlayerIndex]);
   }

}

/*********************************************************************************************
 *
 *                      STATE EIGHT: SPOILAGE
 *********************************************************************************************/
//This check each players holder area and if one
//of the resources is greater then 5 it will take
//the amount and place it in the bank
//If there is not spoilage then it will display
//no spoilage found and do nothing
void MainWindow::spoilage(){
    int spoilageValue,bankAmt;
    listValues <<GOLD << FOOD << WOOD << FAVOR << VICTORY;
    for(int j=0; j<4; j++){
        for(int i=0; i<3;i++){
            spoilageValue = startingOrderOfPlayers[i]->CheckResc(listValues[j]);
            if(spoilageValue > 5){
                spoilageValue = spoilageValue - 5;
               bankAmt = startingOrderOfPlayers[i]->RemoveResc(listValues[j],spoilageValue);
               gameBank->AddResc(listValues[j],bankAmt);
            }
            else{
                messageBox("No spoilage found");
            }
        }
    }
}

/*========================================================
 *
 *      Functions used to display and for connections
 *========================================================/


/********************************************************
 *      Function: Display Functions used throughout
 ********************************************************/

//A basic box that is used for warnings
void MainWindow::setBox(QString player){
    warningBox = new QLabel();
    warningBox->setGeometry(50,50,215,55);
    QPalette palette = warningBox->palette();
    palette.setColor(warningBox->backgroundRole(),QColor(255,102,102,200));
    warningBox->setPalette(palette);
    QString warning1 = "Warning: "; QString warning3 = "'s board not yours";
    QString warning2 = "This is ";
    QString textFormat1 = "<span style = 'font-size:9pt; font-weight:600; "
                          "color:#000000;'>"+warning1+"<br><br>"+warning2+"</span>";
    QString textFormat2 = "<span style = 'font-size:12pt; font-weight:700; "
                          "color:#000000;'>"+player+"</span>";
    QString textFormat3 = "<span style = 'font-size:9pt; font-weight:600; "
                          "color:#000000;'>"+warning3+"</span>";
    QString text = textFormat1+textFormat2+textFormat3;
    warningBox->setText(text);
    warningBox->setWordWrap(true);
    warningBox->setAlignment( Qt::AlignLeft);
    scene->addWidget(warningBox);
}


//A message boxs with a yes or no question returning
//true or false
bool MainWindow::QuestionMessageBox(QString msg){
    message = new QMessageBox();
    message->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    message->setText(msg);
    int ret = message->exec();
    switch(ret){
        case QMessageBox::Yes:
            return true;
        break;
        case QMessageBox::No:
            return false;
        break;
    }
}

//set up pushbuttons size and text
void MainWindow::setPushButton(QPushButton *button,QString str,int x,int y, int w, int h, bool f){
    button->setText(str);
    button->setGeometry(x,y,w,h);
    button->setFlat(f);
    scene->addWidget(button);
}

//Change the color of the pushbuttons if needed to
void MainWindow::modifyPushButton(QPushButton* button,int r,int g, int b, int alpha){
    QPalette palette = button->palette();
    palette.setColor(button->backgroundRole(),QColor(r,g,b,alpha));
    palette.setColor(button->foregroundRole(),Qt::black);
}

//resizing images if needed
void MainWindow::setImage(QString str,int x, int y){
    QPixmap image(str);
    resizeImage(image,x,y);
}

//add the resized image to a scene for backgrounds
void MainWindow::resizeImage(QPixmap a, int x, int y){
    scene->addPixmap(a.scaled(QSize(x,y)));
}


/*
 *          Function: Modifying labels by colors
*/
void MainWindow::modifyLabel(QLabel *label,bool f,int r,int g, int b, int a){
    label->setAutoFillBackground(f);
    QPalette palette = label->palette();
    palette.setColor(label->backgroundRole(),QColor(r,g,b,a));
    palette.setColor(label->foregroundRole(),Qt::white);
    label->setPalette(palette);
}

/*
 *          Function: Modifying labels by size
 */
void MainWindow::labelSize(QLabel *label,int x,int y, int w, int h){
    label->setGeometry(x,y,w,h);
}

/*
 *          Function: Add Text to labels
 */
void MainWindow::addText2Label(QLabel *label,QString Info,QString cardName){
    QString formatText = "<span style='font-size:24pt; font-weight:600; "
                         "color:#FFFFFF;'> Card chosen: </span>"
                         "<span style='font-size:24pt; font-weight:600;"
                         "color:#c4ca14;'>"+cardName+"</span><br>";
    QString formatText2 = "<br><br><br><span style='font-size:12pt; font-weight:600; color:#FFFFFF;'>"+Info+"</span>";
    QString formatting= formatText+formatText2;
    label->setText(formatting);
    label->setWordWrap(true);
    label->setAlignment( Qt::AlignHCenter | Qt::AlignTop);
    scene->addWidget(label);
}

/*
 *          Function: Add Images to labels
 */
void MainWindow::addImage2Label(QLabel *label,QPixmap image,int w, int h){
    label->setPixmap(image.scaled(w,h));
    scene->addWidget(label);
}

/*
 *      Function: Deallocting buttons
 */
void MainWindow::deallocation(QPushButton *button){
    delete button;
}

/*
 *      Function: Connectors
 */

//Various connections that are enabled and disabled based on the current
//state of the system
void MainWindow::connections(int stateFlag){
    if(stateFlag == 1){
         connect(start,SIGNAL(clicked()),this,SLOT(afterTitle()));
    }
    else if(stateFlag == 2){
        //start can not be clicked on
        start->setDisabled(true);
    }
    else if(stateFlag == 3){
        //signals will not be emitted
        disconnect(start,SIGNAL(clicked()),this,SLOT(afterTitle()));
        deallocation(start);
        disconnect(cont,SIGNAL(clicked()),this,SLOT(cultureChosen()));
        deallocation(cont);
    }
}


/*
 *            Function: signal mapping
 *
*                  Allows for a button to emit and integer or something else
*                  if needed.
 */
void MainWindow::signalMapping(QSignalMapper *mapped,QPushButton *button){
    mapped = new QSignalMapper(this);
    connect(button,SIGNAL(clicked()),mapped,SLOT(map()));
    if(button==ai1){
        mapped->setMapping(button,1);
        connect(mapped,SIGNAL(mapped(int)),this,SLOT(switchBoards(int)));
    }
    else if(button==ai2){
         mapped->setMapping(button,2);
         connect(mapped,SIGNAL(mapped(int)),this,SLOT(switchBoards(int)));
    }
    else if(button==userButton){
        mapped->setMapping(button,0);
        connect(mapped,SIGNAL(mapped(int)),this,SLOT(switchBoards(int)));
    }
    else if(button==currentHand){
        mapped->setMapping(button,1);
        connect(mapped,SIGNAL(mapped(int)),this,SLOT(showCurrentHand()));
    }
}

void MainWindow::signalMapping(QSignalMapper *mapped,QShortcut *sc){
    mapped = new QSignalMapper(this);
    connect(sc,SIGNAL(activated()),mapped,SLOT(map()));
    if(sc==a1){
        mapped->setMapping(sc,ai1Location);
        connect(mapped,SIGNAL(mapped(int)),this,SLOT(switchBoards(int)));
    }
    else if(sc==a2){
         mapped->setMapping(sc,ai2Location);
         connect(mapped,SIGNAL(mapped(int)),this,SLOT(switchBoards(int)));
    }
    else if(sc==u){
        mapped->setMapping(sc,userLocation);
        connect(mapped,SIGNAL(mapped(int)),this,SLOT(switchBoards(int)));
    }
 /*   else if(sc==uBoard){
        mapped->setMapping(sc,stateFlag);
        connect(mapped,SIGNAL(mapped(int)),this,SLOT(switchBoards(int)));
    }
    else if(sc==ai1Board){
        mapped->setMapping(sc,stateFlag);
        connect(mapped,SIGNAL(mapped(int)),this,SLOT(switchBoards(int)));
    }
    else if(sc==ai2Board){
        mapped->setMapping(sc,stateFlag);
        connect(mapped,SIGNAL(mapped(int)),this,SLOT(switchBoards(int)));
    }*/
}

void MainWindow::setupShortcuts(){
   a1 = new QShortcut(QKeySequence(Qt::Key_2),this);
   a2 = new QShortcut(QKeySequence(Qt::Key_3),this);
   u = new QShortcut(QKeySequence(Qt::Key_1),this);
   uBoard = new QShortcut(QKeySequence(Qt::Key_4),this);
   ai1Board = new QShortcut(QKeySequence(Qt::Key_5),this);
   ai2Board = new QShortcut(QKeySequence(Qt::Key_6),this);
  a1->setContext(Qt::ApplicationShortcut);
  a2->setContext(Qt::ApplicationShortcut);
  u->setContext(Qt::ApplicationShortcut);
  uBoard->setContext(Qt::ApplicationShortcut);
  ai1Board->setContext(Qt::ApplicationShortcut);
  ai2Board->setContext(Qt::ApplicationShortcut);
  signalMapping(map,a1);
  signalMapping(map,a2);
  signalMapping(map,u);
  signalMapping(map,uBoard);
  signalMapping(map,ai1Board);
  signalMapping(map,ai2Board);
}


//=======================================================
//              Functions Not Used

void MainWindow::removeResourceWidget(){
    scene->removeItem(boarderBox);scene->removeItem(victory);
    scene->removeItem(food); scene->removeItem(gold); scene->removeItem(wood);
    scene->removeItem(favor);scene->removeItem(foodText); scene->removeItem(victoryText);
    scene->removeItem(goldText); scene->removeItem(woodText); scene->removeItem(favorText);
    scene->removeItem(titleText);
}




