#-------------------------------------------------
#
# Project created by QtCreator 2015-02-20T23:37:08
#
#-------------------------------------------------

QT       += core gui
QT       += printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = AgeOfMeth
TEMPLATE = app

CONFIG += resources_big


SOURCES += main.cpp\
        mainwindow.cpp \
    ActionCard.cpp \
    Board.cpp \
    player.cpp \
    holdingarea.cpp \
    bank.cpp \
    tiles.cpp \
    System.cpp

HEADERS  += mainwindow.h \
    ActionCard.h \
    Board.h \
    player.h \
    holdingarea.h \
    bank.h \
    tiles.h \
    System.h

FORMS    += mainwindow.ui

RESOURCES += \
    tiles.qrc \
    victory.qrc \
    cardinit.qrc \
    egypt.qrc \
    greek.qrc \
    norse.qrc \
    other.qrc

