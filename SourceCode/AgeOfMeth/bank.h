#ifndef BANK_H_INCLUDED
#define BANK_H_INCLUDED
#include <QtDebug>

// Enumerated list for the different resource types
enum Resource {
    GOLD,
    WOOD,
    FOOD,
    FAVOR,
    VICTORY
};

class bank{


public:
    bank():victoryPointCubes(30),woods(25),foods(25),favors(25),golds(25){}
    ~bank();
    int getWoods(int);
    int getFoods(int);
    int getFavors(int);
    int getGolds(int);
    int getVictoryCubes(int);
    int getVillagers();
    int getWonderPointCubes(){return wonderPointCubes;}
    int getWinningPointCubes(){return winningPointCubes;}
    int getLargeArmyPointCubes(){return largeArmyPointCubes;}
    int getBuildingPointCubes(){return buildingPointCubes;}
    int totalInBank();
    void setWonderPointCubes(int value){wonderPointCubes+=value;}
    void setWinningPointCubes(int value){winningPointCubes+=value;}
    void setLargeArmyPointCubes(int value){largeArmyPointCubes+=value;}
    void setBuildingPointCubes(int value){buildingPointCubes+=value;}

    int CheckResc(Resource rescType);
    void AddResc(Resource rescType, int amount);
    int RemoveResc(Resource rescType, int amount);

private:
    int victoryPointCubes,woods, foods, favors, golds,temp,villagers;
    int wonderPointCubes=0,winningPointCubes=0,largeArmyPointCubes=0,buildingPointCubes=0;
}; //end bank class

#endif // BANK_H
