#ifndef TILES_H_INCLUDED
#define TILES_H_INCLUDED
#include <QString>
#include <QVector>
#include <iostream>
#include "bank.h"
#include <stdlib.h>
#include <time.h>
using namespace std;

class player;

enum terrainType { FERTILE, FOREST, HILL, MOUNTAIN, DESERT, SWAMP };
enum buildType { HOUSE, WALL, TOWER, STOREHOUSE, MARKET, ARMORY, QUARRY,
                 MONUMENT, GRANARY, GOLDMINT, WOODWORKSHOP, SIEGEENGINEWORKSHOP,
                 GREATTEMPLE, THEWONDER};

class tiles {
public:
    tiles();
    ~tiles();

    // Set terrain/build type
    void setTerrainType(terrainType terrain) { ttype = terrain; }
    void setBuildType(buildType build) { btype = build; }
    void setOwner(player* newOwner) { owner = newOwner; }

    // Function that'll generate random tiles base on user's input
    QVector<QString> randomTile(int user);
    void clearTiles();

    terrainType getTerrainType() { return ttype; }
    buildType getBuildType() { return btype; }

private:
    //QString const dir = "C:/Users/sir_pirate/Desktop/Class/343/Project/soureCodes/AgeOfMeth/pictures/ResourceTiles/";
    //QString const dir = "E:/School/Spring_2015/CECS_343/Git/Git2/cec343_finalproject/soureCodes/AgeOfMeth/pictures/ResourceTiles/";
    QString const dir = ":/pictures/ResourceTiles/";
    terrainType ttype;
    buildType btype;
    player* owner;
    QVector<QString> tileHolder;

}; // End of tiles class

class resourceTiles : public tiles {
public:
    virtual void generateResources();

}; // End of resource tiles class

class buildTiles : public tiles {
public:
    virtual void specialAbility();

}; // End of build tiles class

#endif
