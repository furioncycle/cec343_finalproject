#include "bank.h"


bank::~bank()
{

}

int bank::totalInBank(){
    int total;
    total = foods + victoryPointCubes +
            woods + favors + golds;
    return total;
}

int bank::getWoods(int amt){
      temp = woods;
      if((temp-amt) < 0)
          return -1;
      else{
        woods -= amt;
        return amt;
    }
}
int bank::getFoods(int amt){
    temp = foods;
    if((temp-amt) < 0)
        return -1;
    else{
        foods -= amt;
      return amt;
    }
}
int bank::getFavors(int amt){
    temp = favors;
    if((temp-amt) < 0)
        return -1;
    else
        favors -= amt;
        return amt;
}
int bank::getGolds(int amt){
    temp = golds;
    if((temp-amt) < 0)
        return -1;
    else{
        golds -= amt;
        return amt;
    }
}
int bank::getVictoryCubes(int amt){
    temp = victoryPointCubes;
    if((temp-amt) < 0)
        return -1;
    else{
        victoryPointCubes -= amt;
        return  amt;
    }
}

int bank::getVillagers(){

    return villagers = villagers;
}

/*
    Returns the amount of the indicated resource in the bank. A return
    value of -1 indicates the wrong type was inputted.

    @param rescType The type of resource to check.
    @return The amount of the indicated resource in the bank.
*/
int bank::CheckResc(Resource rescType) {
    // Return the indicated amount, if anything goes wrong return -1
    switch(rescType) {
    case WOOD:
        return woods;
        break;
    case FOOD:
        return foods;
        break;
    case FAVOR:
        return favors;
        break;
    case GOLD:
        return golds;
        break;
    case VICTORY:
        return victoryPointCubes;
        break;
    default:
        return -1;
    }

    return -1;
}


/*
    Adds the indicated amount of resource to the bank.

    @param rescType The type of resource to be added.
    @param amount The amount of resource to be added.
*/
void bank::AddResc(Resource rescType, int amount) {
    switch(rescType) {
    case WOOD:
        woods += amount;
        break;
    case FOOD:
        foods += amount;
        break;
    case FAVOR:
        favors += amount;
        break;
    case GOLD:
        golds += amount;
        break;
    case VICTORY:
        victoryPointCubes += amount;
        break;
    default:
        return;
    }
}


/*
    Removes the indicated amount of resource from the bank. When there is not
    enough resources left, the resource will be depleted and the actual amount
    will be the value returned.

    @param rescType The type of resource to be removed.
    @param amount The amount of resource to be removed.
    @return The total amount removed from the bank.
*/
int bank::RemoveResc(Resource rescType, int amount) {
    int depletedAmount = 0;

    // if there is not enough available in the bank, then return the amount in the bank
    switch(rescType) {
    case WOOD:
        if(woods < amount) {
            depletedAmount = woods;
            woods = 0; // deplete bank
            return depletedAmount; // return amount
        }
        else {
            woods -= amount; // enough in bank
        }
        break;
    case FOOD:
        if(foods < amount) {
            depletedAmount = foods;
            foods = 0;
            return depletedAmount;
        }
        else {
            foods -= amount;
        }
        break;
    case FAVOR:
        if(favors < amount) {
            depletedAmount = favors;
            favors = 0;
            return depletedAmount;
        }
        else {
            favors -= amount;
        }
        break;
    case GOLD:
        if(golds < amount) {
            depletedAmount = golds;
            golds = 0;
            return depletedAmount;
        }
        else {
            golds -= amount;
        }
        break;
    case VICTORY:
        if(victoryPointCubes < amount) {
            depletedAmount = victoryPointCubes;
            victoryPointCubes = 0;
            return depletedAmount;
        }
        else {
            victoryPointCubes -= amount;
        }
        break;
    default:
        return -1; // something went wrong
    }

    return amount; // all requested resources removed
}

